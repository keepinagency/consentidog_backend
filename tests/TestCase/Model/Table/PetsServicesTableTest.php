<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PetsServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PetsServicesTable Test Case
 */
class PetsServicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PetsServicesTable
     */
    public $PetsServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PetsServices',
        'app.Pets',
        'app.Services'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PetsServices') ? [] : ['className' => PetsServicesTable::class];
        $this->PetsServices = TableRegistry::getTableLocator()->get('PetsServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PetsServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
