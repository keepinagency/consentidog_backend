<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Movement[]|\Cake\Collection\CollectionInterface $movements
 */

$fecha = date("Y/m/d");
//resto 1 día
$dia = date("Y/m/d",strtotime($fecha."- 1 days")); 
//resto 1 semana
$semana = date("Y/m/d",strtotime($fecha."- 1 week")); 
//resto 1 mes
$mes = date("Y/m/d",strtotime($fecha."- 1 month"));

$total_periodo=array();
?>

<div class="col-12 text-right d-flex flex-row row">
    <div class="col-9 text-left">
        <form method="get" accept-charset="utf-8" role="form" action="movements" id="frmbuscar">
            <div class="input-group no-border p-1 bg-light">
                    <input name="q" id="q" type="text" 
                        value="<?= $q;?>" class="form-control p-0" placeholder="Buscar..."
                        onKeyUp="if (event.keyCode === 13) {
                                        document.getElementById('frmbuscar').submit();
                                    }">
                    <select id="consulta" name="consulta" class="form-control ml-1 p-0" class="form-control" 
                        onKeyUp="if (event.keyCode === 13) 
                            {document.getElementById('frmfecha').submit();}">
                        <?php
                            $todossel = '';
                            if ($consulta == '*'){
                                $todossel = 'selected';
                            }
                        ?>
                        <option value="0" <?= $todossel;?>>Todos</option>
                        <?php foreach ($movementtypes as $movementtype): ?>
                            <?php
                                $optsel = '';
                                if ($consulta == $movementtype->id){
                                    $optsel = 'selected';
                                }
                            ?>
                            <option value="<?= $movementtype->id; ?>" <?= $optsel;?>>
                                <?= $movementtype->display_name; ?>
                            </option>
                        <?php endforeach; ?>    
                    </select>
                    <input name="desde" id="desde" type="text" 
                        value="<?= $desde;?>" class="form-control p-0 datetimepicker" placeholder="Desde..."
                        onKeyUp="if (event.keyCode === 13) {
                                        document.getElementById('frmbuscar').submit();
                                    }">
                    <input name="hasta" id="hasta" type="text" 
                        value="<?= $hasta;?>" class="form-control p-0 datetimepicker" placeholder="Hasta..."
                        onKeyUp="if (event.keyCode === 13) {
                                        document.getElementById('frmbuscar').submit();
                                    }">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="nc-icon nc-zoom-split" 
                            onClick="document.getElementById('frmbuscar').submit();"></i>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <div class="col-3 text-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add Movement'),['controller' => 'Movements' ,'action' => 'add'], 
            ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ;?>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('concept') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('movement_type_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('currency_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('service') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                            <!--th scope="col"><?= $this->Paginator->sort('modified') ?></th-->
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($movements as $movement): ?>
                        <tr>
                            <td><?= $this->Number->format($movement->id) ?></td>
                            <td><?= h($movement->concept) ?></td>
                            <td><?= $this->Number->format($movement->amount); ?></td>
                            <?php
                                $armonto = array( $movement->currency->name => $movement->amount);
                                // print_r($armonto);
                                // echo "<br>";
                                if (array_key_exists($movement->currency->name, $total_periodo)) {
                                    $oldvalor = $total_periodo[$movement->currency->name];
                                    $total_periodo[$movement->currency->name]=$oldvalor+$movement->amount;
                                }else{
                                    $total_periodo += array($movement->currency->name=>$movement->amount);
                                }
                            ?>
                            <td><?= $movement->has('movement_type') ? $this->Html->link($movement->movement_type->display_name, ['controller' => 'MovementTypes', 'action' => 'view', $movement->movement_type->id]) : '' ?></td>
                            <td><?= $movement->has('currency') ? $this->Html->link($movement->currency->name, ['controller' => 'Currencies', 'action' => 'view', $movement->currency->id]) : '' ?></td>

                            <!--td><?= $movement->has('pets_service') ? $this->Html->link($movement->pets_service->id, 
                                ['controller' => 'PetsServices', 
                                'action' => 'view', $movement->pets_service->id]) : '' ?>
                            </td-->
                            
                            <td> <?= h($movement->pets_service->pet->name)."<br/>"
                                    .h($movement->pets_service->service->name)."<br/>"
                                    .h($movement->pets_service->start_date)?></td>

                            <td><?= h($movement->created) ?></td>
                            <!--td><?= h($movement->modified) ?></td-->
                            <td class="actions">
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-info-circle"></i>', ['action' => 'view', $movement->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-edit"></i>', ['action' => 'edit', $movement->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="btnact fas fa-trash"></i>', ['action' => 'delete', $movement->id], ['escape' => false , 'confirm' => __('Are you sure you want to delete # {0}?', $movement->concept)])?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination p-2">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
            <li class="flex-fill text-right pb-3 pr-4 m-0 mr-2 align-top" >
                <u>Total Montos:</u>&nbsp;
                <?php 
                //print_r($total_periodo);
                foreach ($total_periodo as $moneda => $total_monto){
                    //print_r($total_monto);
                    echo "<b>{$total_monto}</b> {$moneda} &nbsp;&nbsp;";
                } ?>
            </li>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        
    </div>
</div>
<script>
$(function(){
        $('#desde').datetimepicker ({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fas fa-trash',
            close: 'fa fa-remove'
        },
        format: "DD/MM/Y"
        });

        $('#hasta').datetimepicker ({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fas fa-trash',
            close: 'fa fa-remove'
        },
        format: "DD/MM/Y"
        });
});
</script>