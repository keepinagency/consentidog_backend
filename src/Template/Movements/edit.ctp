<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Movement $movement
 */
use Cake\Routing\Router;
?>
<div class="col-md-8 ">
    <div class="card card-user h-100">
        <div class="card-body">
            <?= $this->Form->create($movement) ?>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <?php echo $this->Form->control('concept'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('movement_type_id', ['options' => $movementTypes]); ?>
                    </div>
                </div>
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('currency_id', ['options' => $currencies]); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?php echo $this->Form->control('amount'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="card card-user h-100">
        <div class="card-body">
            <div class="col-md-12">
                <div class="form-group">
                    <?php 
                    echo $this->Form->control('masked_start_date', [
                        'id' => 'diaservicios', 
                        'label' => __('Day').' '.__('Start Date').' '.__('Associated Service'),
                        'class' => 'datetimepicker',
                        'value' =>$movement->pets_service->start_date]); 
                    ?>
                </div>
            </div>
            <div class="col-md-12" >
                <div class="form-group">
                    <?php 
                    
                    echo $this->Form->control('pet_id', 
                        ['id'=>'mascota', 
                         'options' => $pets,
                         'disabled'=> 'disabled',
                         'value' => $movement->pets_service->pet_id,
                         'empty' => __('Seleccione una mascota')]); 
                    /*
                    echo $this->Form->control('mascota',
                        ['options' => [$pets->pet->name], 
                        ['value'=>$pets->pet_id] ]); 
                    */
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <?php 
                    echo $this->Form->control('servicios', 
                        ['id'=>'servicios', 
                        'options' => $services,
                        'disabled'=> 'disabled',
                        'value' => $movement->pets_service->service_id,
                        'empty' => __('Seleccione un servicio')]); 
                    /*
                    echo $this->Form->control('servicios',
                                ['options' => [$services->service->name], 
                                ['value'=>$services->pet_id] ]); 
                    */
                    ?>
                    
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('pets_service_id', 
                        ['id'=>'pets_service_id','type' => 'hidden']); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-12 text-right pr-4 pt-2"> 
    <?= $this->Form->button(__('Save').' '.__('Movement'),[
            'class'=>'btn btn-primary btn-round',
            'type' =>'submit'
            ]) ?>
    <?= $this->Form->end() ?>
</div>

<script>
$(function(){
        $('#diaservicios').datetimepicker ({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fas fa-trash',
            close: 'fa fa-remove'
        },
        format: "DD/MM/Y"
        });
});
$("#diaservicios").on('blur',function() {
    var dia = $(this).val();
    $("#mascota").find('option').remove();
    $("#servicios").find('option').remove();
    $('#pets_service_id').val('0');
    if (dia) {
        var dataString = 'fecha='+ dia;
        $.ajax({
            dataType:'json',
            type: "POST",
            url: '<?php echo Router::url(array("controller" => "Movements", "action" => "get-pets")); ?>?' ,
            data: dataString,
            cache: false,
            beforeSend: function (xhr) { 
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },  
            success: function(html) {
                $("#mascota").prop( "disabled", false );
                $('<option>').val('0').text('<?= __('Seleccione mascota') ?>').appendTo($("#mascota"));
                $.each(html, function(key, value) { 
                    $('<option>').val(value['pet']['id']).text(value['pet']['name']).appendTo($("#mascota"));
                });
            } 
        });
    }
});
$("#mascota").on('change',function() {
    var idpet = $(this).find('option:selected').val();
    var fecha = $('#diaservicios').val();
    $("#servicios").find('option').remove();
    if (idpet) {
        var dataString = 'idpet='+ idpet + '&fecha=' + fecha;
        $.ajax({
            dataType:'json',
            type: "POST",
            url: '<?php echo Router::url(array("controller" => "Movements", "action" => "get-services")); ?>' ,
            data: dataString,
            cache: false,
            processData: true,
            beforeSend: function (xhr) { 
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            }, 
            success: function(html) {
                $("#servicios").prop( "disabled", false );
                $('<option>').val('0').text('<?= __('Selecciones servicio') ?>').appendTo($("#servicios"));
                $.each(html, function(key, value) { 
                    $('<option>').val(value['id']).text(value['service']['name']).appendTo($("#servicios"));
                });
            } 
        })
        .fail(function() {
            alert( "error" );
        });
    }
});

$("#servicios").on('change',function() {
    var idpetserv = $(this).find('option:selected').val();
    $('#pets_service_id').val( idpetserv );
});
</script>