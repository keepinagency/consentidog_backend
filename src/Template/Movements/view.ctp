<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Movement $movement
 */
?>
<div class="col-md-8">
    <div class="card card-user h-100">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label><?= __('Fecha') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($movement->created) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Concept') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Concept') ?>" value="<?= h($movement->concept) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><?= __('Movement Type') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Service') ?>" value="<?= h($movement->movement_type->display_name) ?>">
                    </div>
                </div>
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><?= __('Currency') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Currency') ?>" value="<?= h($movement->currency->name) ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?= __('Amount') ?></label>
                        <input type="text" class="form-control text-right font-weight-bold" disabled="" placeholder="<?= __('Amount') ?>" value="<?= $this->Number->format($movement->amount) ?>">
                    </div>
                </div>
            </div>
                
            <div class="row p-0 m-0">
                <div class="update ml-auto mr-auto  pt-2">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit Movement'), ['controller' => 'Movements' ,'action' => 'edit', $movement->id], [
                                    'class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 ">
    <div class="card card-user h-100">
        <div class="card-body">
        <?php
        if ($movement->pets_service->id){
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Day').' '.__('Start Date') ?></label>
                        <input type="text" class="form-control" disabled="" value="<?= h($movement->pets_service->start_date)?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Pet') ?></label>
                        <input type="text" class="form-control" disabled="" value="<?= h($movement->pets_service->pet->name)?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Associated Service') ?></label>
                        <!--input type="text" class="form-control" disabled=""?>" value="<?= h($movement->pets_service->id)?>"-->
                        <input type="text" class="form-control" disabled="" value="<?= h($movement->pets_service->service->name)?>">
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 pt-2 text-center">
                    <?= $this->Html->link('<i class="btnact fas fa-info-circle"></i>&nbsp;&nbsp;'.__('View').' '.__('Service'), 
                            ['controller' => 'PetsServices', 
                             'action' => 'view', $movement->pets_service->id],
                             ['class' => 'btn btn-primary btn-round',
                              'escape' => false]) ?>
                </div>
            </div>
            <?php
        }else{
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Service')." ".__('asociado') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="No posee" value="">
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        </div>
    </div>
</div>