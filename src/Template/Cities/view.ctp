<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\City $city
 */
?>
<div class="col-md-10">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Name') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($city->name) ?>">
                        </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('City') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($city->state->name) ?>">
                    </div>
                </div>
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= $this->Number->format($city->id) ?>">
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($city->created) ?>">
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($city->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit City'), ['controller' => 'Cities' ,'action' => 'edit', $city->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>    
        </div>
    </div>
</div>
