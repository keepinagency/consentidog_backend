<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Country $country
 */
?>

<div class="col-md-10">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($country->id) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Country') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($country->name) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($country->created) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($country->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit Country'), ['controller' => 'Countries' ,'action' => 'edit', $country->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
            <h4><?= __('Related States') ?></h4>
            <div class="row related">
                <?php if (!empty($country->states)): ?>
                <?php foreach ($country->states as $states): ?>
                    <div class="col-md-1 pr-1">
                        <div class="form-group">
                            <label><?= __('Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($states->id) ?>">
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label><?= __('Name') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($states->name) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Country Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Country Id') ?>" value="<?= h($states->country_id) ?>">
                        </div>
                    </div>
                    <div class="col-md-3 pr-1">
                        <div class="actions">
                            <label><?= __('Actions') ?></label>
                        </div>
                        <div>
                            <?= $this->Html->link('<i class="fa fa-info-circle"></i>', ['controller' => 'States', 'action' => 'view', $states->id], ['escape' => false]) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'States', 'action' => 'edit', $states->id], ['escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'States', 'action' => 'delete', $states->id], ['escape' => false], ['confirm' => __('Are you sure you want to delete # {0}?', $states->id)]) ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
