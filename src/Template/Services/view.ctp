<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
?>
<div class="col-md-10">
    <div class="card card-user">
        <div class="card-header">
            <h5 class="card-title"><?= h($service->name) ?></h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-1 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($service->id) ?>">
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label><?= __('Name') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($service->name) ?>">
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($service->created) ?>">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($service->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit service'), ['controller' => 'Services' ,'action' => 'edit', $service->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>