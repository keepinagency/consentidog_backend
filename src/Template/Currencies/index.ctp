<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Currency[]|\Cake\Collection\CollectionInterface $currencies
 */
?>
<div class="col-md-12 text-right">
    <?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add Currency'), ['controller' => 'Currencies' ,'action' => 'add'], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('display_name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($currencies as $currency): ?>
                        <tr>
                            <td><?= $this->Number->format($currency->id) ?></td>
                            <td><?= h($currency->name) ?></td>
                            <td><?= h($currency->display_name) ?></td>
                            <td><?= h($currency->created) ?></td>
                            <td><?= h($currency->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-info-circle"></i>', ['action' => 'view', $currency->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $currency->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['action' => 'delete', $currency->id],['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $currency->name)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
