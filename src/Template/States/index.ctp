<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\State[]|\Cake\Collection\CollectionInterface $states
 */
?>
<div class="col-md-12 text-right">
<?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add State'), ['controller' => 'States' ,'action' => 'add'], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('country_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </thead>
                    <tbody>
                        <?php foreach ($states as $state): ?>
                        <tr>
                            <td><?= $this->Number->format($state->id) ?></td>
                            <td><?= h($state->name) ?></td>
                            <td><?= $state->has('country') ? $this->Html->link($state->country->name, ['controller' => 'Countries', 'action' => 'view', $state->country->id]) : '' ?></td>
                            <td><?= h($state->created) ?></td>
                            <td><?= h($state->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-info-circle"></i>', ['action' => 'view', $state->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $state->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['action' => 'delete', $state->id],['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $state->name)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>