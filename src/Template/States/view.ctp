<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\State $state
 */
?>

<div class="col-md-10">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Name') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($state->name) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Country') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($state->country->name) ?>">
                    </div>
                </div>
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($state->id) ?>">
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($state->created) ?>">
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($state->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit State'), ['controller' => 'States' ,'action' => 'edit', $state->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>    
            <h4><?= __('Related Cities') ?></h4>
            <div class="row related">
                <?php if (!empty($state->cities)): ?>
                <?php foreach ($state->cities as $cities): ?>
                    <div class="col-md-1 pr-1">
                        <div class="form-group">
                            <label><?= __('Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($cities->id) ?>">
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label><?= __('Name') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($cities->name) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('State Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Country Id') ?>" value="<?= h($cities->state_id) ?>">
                        </div>
                    </div>
                    <div class="col-md-3 pr-1">
                        <div class="actions">
                            <label><?= __('Actions') ?></label>
                        </div>
                        <div>
                            <?= $this->Html->link('<i class="fa fa-info-circle"></i>', ['controller' => 'Cities', 'action' => 'view', $cities->id], ['escape' => false]) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Cities', 'action' => 'edit', $cities->id], ['escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'Cities', 'action' => 'delete', $cities->id], ['escape' => false], ['confirm' => __('Are you sure you want to delete # {0}?', $cities->id)]) ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>