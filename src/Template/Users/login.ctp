
    <!--h3 class="title mx-auto"><?= __('Welcome') ?></h3-->
    <div class="text-center pb-2">
        <?php
            echo  $this->Html->image('cd_isologo_web.png', [
                        'class' => '',
                        'width' => '300px'
                    ]); 
        ?>
        <h6 class=" mx-auto pt-2"><?= __('BackEnd Control') ?></h6>
    </div>
    
    <?= $this->Form->create() ?>
        <?= $this->Form->control('username',) ?>
        <?= $this->Form->control('password') ?>
        <?= $this->Form->button(__('Login'), ['class' => 'btn btn-danger btn-block btn-round']) ?>
    <?= $this->Form->end() ?>
    <div class="forgot">
        <a href="#" class="btn btn-link btn-danger"><?= __('Forgot password?') ?></a>
    </div>
