<?php
use Cake\Core\Configure;
?>
            <div class="card-body ">
                <div class="row p-0">
                    <div class="col-1 col-md-4 ">
                        <div class="icon-small text-left icon-warning">
                            <i class="far fa-calendar-alt text-danger"></i><br>
                            <?php
                                $fecha = DateTime::createFromFormat('Y-m-j', $dia);
                                echo date_format($fecha, 'd/m/Y');
                            ?>
                        </div>
                    </div>
                    <div class="col-11 col-md-8">
                        <div class="numbers">
                            <p class="card-category"><?= __('Servicios del Día') ?></p>
                            <p class="card-title"><?= $total_pets_service_confirmed ?></p>
                        </div>
                    </div>
                </div>
            
                <div class="row pl-1 pr-1">
                    <table class="table table-striped">
                        <thead>
                            <th scope="col" class="col-5"><?= $this->Paginator->sort('pet_id') ?></th>
                            <th scope="col text-center"><?= $this->Paginator->sort('start_date') ?></th>
                            <th scope="col text-center"><?= $this->Paginator->sort('end_date') ?></th>
                        </thead>
                        <tbody>
                        <?php if (count($petsServices)>0): ?>
                            <?php
                                
                                //echo '<pre>';
                                //print_r($petsServices);
                                //die();
                                
                            ?>
                            <?php foreach ($petsServices as $petsService): ?>
                                <tr>
                                    <td><?= $petsService['PetName'] ?>
                                        <br>
                                        <?= $petsService['ServiceName'] ?>
                                    </td>
                                    <?php
                                        $fecha_ini = DateTime::createFromFormat('Y-m-j H:i:s', $petsService['start_date']);
                                        $varclass_ini = "";
                                        if (date_format($fecha_ini, 'd/m/Y') == date_format($fecha, 'd/m/Y')){
                                            //$varclass_ini = "border border-success";
                                        }
                                    ?>
                                    <td class="text-center col-2 <?= $varclass_ini;?>">
                                        <!--?= $petsService['start_date'] ?-->
                                        <?php
                                            
                                            echo date_format($fecha_ini, 'd/m/Y h:i:s');
                                        ?>
                                    </td>
                                    <?php
                                        $fecha_fin = DateTime::createFromFormat('Y-m-j H:i:s', $petsService['end_date']);
                                        $varclass = "";
                                        //if (date_format($fecha_fin, 'd/m/Y') == date_format($fecha, 'd/m/Y')){
                                            if ((date_format($fecha_fin, 'd/m/Y') == date('d/m/Y')) and
                                                !$petsService['executed']){
                                            $varclass = "border bg-danger";
                                        }
                                        if ((date_format($fecha_fin, 'd/m/Y') == date('d/m/Y',strtotime("+1 days"))) and 
                                            !$petsService['executed']){
                                            $varclass = "border bg-warning";
                                        }
                                    ?>
                                    <td class="text-center col-2 <?= $varclass;?>">
                                        <!--?= $petsService['end_date'] ?-->
                                        <?php
                                            echo date_format($fecha_fin, 'd/m/Y h:i:s');
                                        ?>
                                    </td>
                                    <td class="actions text-center"> 
                                        <?= $this->HTML->link('<i class="fa fa-info-circle"></i>', 
                                            ['controller' => 'PetsServices', 'action' => 'view', $petsService['id']], 
                                                ['escape' => false])?>
                                        <?php
                                            if ($petsService['confirmed']){
                                            ?>
                                            <i class="fas fa-check"></i>
                                            <?php
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>