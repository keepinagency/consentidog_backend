<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="col-12 text-right d-flex flex-row row">
    <div class="col-6 text-left">
        <form method="get" accept-charset="utf-8" role="form" action="users" id="frmbuscar">
            <div class="input-group no-border">
                <input name="q" id="q" type="text" 
                    value="" class="form-control" placeholder="Buscar..."
                    onKeyUp="if (event.keyCode === 13) {
                                    document.getElementById('frmbuscar').submit();
                                }">
            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split" 
                        onClick="document.getElementById('frmbuscar').submit();"></i>
                </div>
            </div>
            </div>
        </form>
    </div>
    <div class="col-6 text-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add User'), ['controller' => 'Users' ,'action' => 'add'], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
    </div>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                            <!--th scope="col"><?= $this->Paginator->sort('mobile') ?></th-->
                            <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= h($user->name) ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= h($user->username) ?></td>
                            <td class="text-center">
                                <?= h($user->phone) ?><br>
                                <a href="https://wa.me/<?= h($user->mobile) ?>" target="blank"><?= h($user->mobile) ?></a>
                            </td>
                            <!--td><?= h($user->mobile) ?></td-->
                            <td><?= $user->has('role') ? $this->Html->link($user->role->display_name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                            <td class="text-center"><?= $user->active ? __('Yes') : __('No'); ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-info-circle"></i>', ['action' => 'view', $user->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-edit"></i>', ['action' => 'edit', $user->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="btnact fas fa-trash"></i>', ['action' => 'delete', $user->id], ['escape' => false , 'confirm' => __('Are you sure you want to delete # {0}?', $user->name)])?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
