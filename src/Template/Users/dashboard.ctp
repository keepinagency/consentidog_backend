<?php
use Cake\Core\Configure;
?>
<script>

/*$(document).ready(function() {
}
*/
</script>
<style>

	#wrap {
		width: 100%; /*1100px;*/
		margin: 0 auto;
		}

	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		text-align: left;
		}

	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
		}

	.external-event { /* try to mimick the look of a real event */
		margin: 10px 0;
		padding: 2px 4px;
		background: #3366CC;
		color: #fff;
		font-size: .85em;
		cursor: pointer;
		}

	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
		}

	#external-events p input {
		margin: 0;
		vertical-align: middle;
		}

	#calendar {
/* 		float: right; */
        margin: 0 0;
		width: 100%;
		background-color: #FFFFFF;
		  border-radius: 6px;
        box-shadow: 0 1px 2px #C3C3C3;
		}

</style>
<div class="row col-md-12 m-0 p-0">

<div class="col-lg-8 col-md-8 " >
    <div id='wrap' class='pt-0'>
        <div id='calendar'></div>
    </div>
</div>

<div class="col-lg-4 col-md-4 m-0 p-0">
        <!--button type="button">Click Me!</button-->
        <div class="card card-stats m-0 p-0" id="carddetalle">
            <div class="card-body ">
                <div class="row p-0">
                    <div class="col-1 col-md-4 ">
                        <div class="icon-medium text-left icon-warning">
                            <i class="far fa-calendar-alt text-danger"></i><br>
                            <?php
                                //$fecha = DateTime::createFromFormat('Y-m-j', $dia);
                                //echo date_format($fecha, 'd/m/Y');
                                echo date('d/m/Y');
                            ?>
                        </div>
                    </div>
                    <div class="col-11 col-md-8">
                        <div class="numbers">
                            <p class="card-category"><?= __('Servicios En Curso') ?></p>
                            <p class="card-title"><?= $total_pets_service_confirmed ?></p>
                        </div>
                    </div>
                </div>
            
                <div class="row pl-1 pr-1">
                    <table class="table table-striped">
                        <thead>
                            <th scope="col" class="col-5"><?= $this->Paginator->sort('pet_id') ?></th>
                            <th scope="col text-center"><?= $this->Paginator->sort('start_date') ?></th>
                            <th scope="col text-center"><?= $this->Paginator->sort('end_date') ?></th>
                        </thead>
                        <tbody>
                        <?php if (count($petsServicesDay)>0): ?>
                            <?php foreach ($petsServicesDay as $petsService): ?>
                                <?php
                                
                                //print_r($petsService);

                                $fecha_fin = DateTime::createFromFormat('j/m/y H:i', $petsService['end_date']);
                                $varclasstr = "";
                                $fechaact = date('m/d/Y');
                                
                                $fechafin_correcta = date_format($fecha_fin, 'm/d/Y');
                                $timefin = strtotime($fechafin_correcta);
                                $timehaceunmes = strtotime("-30 day");

                                if ($timehaceunmes > $timefin){
                                    //$varclasstr = "border bg-danger";
                                }
                                ?>
                                <tr class="<?= $varclasstr;?>">
                                    <td>
                                        <?= h($petsService['PetName']) ?><br>
                                        <?= $petsService['ServiceName'] ?>
                                    </td>
                                    <?php
                                        $fecha_ini = DateTime::createFromFormat('Y-m-j H:i:s', $petsService['start_date']);
                                        $varclass_ini = "";
                                        if (date_format($fecha_ini, 'd/m/Y') == date_format($fecha, 'd/m/Y')){
                                            //$varclass_ini = "border border-success";
                                        }
                                    ?>
                                    <td class="text-center col-2 <?= $varclass_ini;?>">
                                        <!--?= $petsService['start_date'] ?-->
                                        <?php
                                            
                                            echo date_format($fecha_ini, 'd/m/Y h:i:s');
                                        ?>
                                    </td>
                                    <?php
                                        $fecha_fin = DateTime::createFromFormat('Y-m-j H:i:s', $petsService['end_date']);
                                        $varclass = "";
                                        //if (date_format($fecha_fin, 'd/m/Y') == date_format($fecha, 'd/m/Y')){
                                            if ((date_format($fecha_fin, 'd/m/Y') == date('d/m/Y')) and
                                                !$petsService['executed']){
                                            $varclass = "border bg-danger";
                                        }
                                        if ((date_format($fecha_fin, 'd/m/Y') == date('d/m/Y',strtotime("+1 days"))) and 
                                            !$petsService['executed']){
                                            $varclass = "border bg-warning";
                                        }
                                    ?>
                                    <td class="text-center col-2 <?= $varclass;?>">
                                        <!--?= $petsService['end_date'] ?-->
                                        <?php
                                            echo date_format($fecha_fin, 'd/m/Y h:i:s');
                                        ?>
                                    </td>
                                    <td class="actions text-center"> 
                                        <?= $this->HTML->link('<i class="fa fa-info-circle"></i>', 
                                            ['controller' => 'PetsServices', 'action' => 'view', $petsService['id']], 
                                                ['escape' => false])?>
                                        <?php
                                            if ($petsService['confirmed']){
                                            ?>
                                            <i class="fas fa-check"></i>
                                            <?php
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>


<script>
    $(document).ready(function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        /*  className colors

        className: default(transparent), important(red), chill(pink), success(green), info(blue)

        */


        /* initialize the external events
        -----------------------------------------------------------------*/

        $('#external-events div.external-event').each(function() {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });


        /* initialize the calendar
        -----------------------------------------------------------------*/

        var calendar =  $('#calendar').fullCalendar({
            header: {
                left: 'title',
                center: 'agendaDay,agendaWeek,month',
                right: 'prev,next today'
            },
            editable: false,
            firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: false,
            defaultView: 'month',
            eventOrder: 'color',
            axisFormat: 'h:mm',
            columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
            allDaySlot: false,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                }
                calendar.fullCalendar('unselect');
            },
            droppable: false, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            },
            
            events: [
            <?php
            if (count($petsServicesFull)>0):
                foreach ($petsServicesFull as $petsService): 
                    $anio = $petsService->start_date->year;
                    $mes = $petsService->start_date->month;
                    $dia = $petsService->start_date->day;
                    //$hora = $petsService->start_date->hour;
                    //$min = $petsService->start_date->minute;

                    $anio_e = $petsService->end_date->year;
                    $mes_e = $petsService->end_date->month;
                    $dia_e = $petsService->end_date->day;
                    //$hora_e = $petsService->end_date->hour;
                    //$min_e = $petsService->end_date->minute;

                    //echo $petsService->confirmed." ".$petsService->executed;

                    $diffdates = $petsService->end_date - $petsService->start_date;
                    $diffdates = (array) date_diff($petsService->start_date, $petsService->end_date);

                    //echo "//".$petsService->service->color;
                    if ($diffdates['d']>0) {

                        for ($i = 0; $i <= $diffdates['d']; $i++){

                            $start_date_mdy=$mes."/".$dia."/".$anio;

                            $fecha_1 = date("d/m/Y",strtotime($start_date_mdy." + ".$i." days")); 

                            $fecha_1 = explode("/",$fecha_1);

                            
                            $anio_r = $fecha_1[2];
                            $mes_r = intval($fecha_1[1]);
                            $dia_r = $fecha_1[0];
                              
                            ?>
                            {
                                id: <?= $petsService->pet->id ?>,
                                
                                title: '<?= h($petsService->pet->name) ?>',
                                start: new Date('<?= $anio_r ?>','<?= $mes_r-1 ?>','<?= $dia_r ?>'),
                                allDay: true,
                                className: 'confirmado',
                                textColor: 'white',
                                display: 'background',
                                backgroundColor: '<?= $petsService->service->color;?>',
                                diffdates: <?= $diffdates['d'];?>
                                
                            },
                            <?php
                            
                        }
                    }else{
                        ?>
                            {
                                id: <?= $petsService->pet->id ?>,
                                title: '<?= h($petsService->pet->name) ?>',
                                start: new Date('<?= $anio ?>','<?= $mes-1 ?>','<?= $dia ?>','<?= $hora ?>','<?= $min ?>'),
                                end: new Date('<?= $anio_e ?>','<?= $mes_e-1 ?>','<?= $dia_e ?>','<?= $hora_e ?>','<?= $min_e ?>'),
                                allDay: true,
                                className: 'confirmadoaaa',
                                textColor: 'white',
                                display: 'background',
                                backgroundColor: '<?= $petsService->service->color;?>',
                                diffdays: <?= $diffdates['d']?>
                                //color: '<?= ($petsService->executed ? "green" : ($petsService->confirmed ? "orange" : "red"));?>'
                                
                            },
                        <?php
                    } 
                    $i =0;
                endforeach;
            endif;
            ?>
            ],
            /*
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1)
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false,
                    className: 'info'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false,
                    className: 'info'
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    className: 'important'
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    className: 'important'
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false,
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/',
                    className: 'success',
                    color: 'green'
                }
            ],*/
        });
        

        /*var elems = document.querySelectorAll('.fc-buttom');

        for (var i=elems.length; i--;) {
            elems[i].addEventListener('click', habclickdias, false);
        }


        habclickdias();
        
        function habclickdias() {
        */

        

            var actudetalle = {
                mandaAlert: function(){
                    alert('funciona');
                }
            }

            //var ele = $( '.calendar' );
            //ele.on( 'dayClick', actudetalle.addClick.bind( actudetalle ) );

            /*
            // sobreescribe el evento
            var elems = document.querySelectorAll('.fc-day-number');

            for (var i=elems.length; i--;) {
                //elems[i].addEventListener('click', fn, false);
                elems[i].addEventListener('click', actudetalle.mandaAlert.bind( actudetalle ));
            }
            */

            /*function fn() {
                
                //if ( this.name == 'fc-day-number' ) {

                    //alert(this.innerHTML); //número de mes
                    mes = document.getElementsByClassName("fc-header-title")[0].firstChild;
                    
                    alert(mes.innerHTML);

                //} else if ( this.name == 'link2' ) {

                //    console.log('This is link2');

                //}
            }*/
        /*}
        */
    });

    function clickdias(fecha) {
        $.ajax({
            type: "GET",
            crossDomain: true,
            //async: false,
            url: "<?php echo $linkdayservices; ?>/"+fecha,
            beforeSend: function() {
                    $('#carddetalle').html('<div class="d-flex justify-content-center align-items-center" style="height:30vh;"><?= __('Loading')?>... </div>');
            },
            success: function(msg){
            $('#carddetalle').html(msg);
            }
        });
    }
</script>