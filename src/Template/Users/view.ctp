<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="col-md-4">
    <div class="card card-user">
        <div class="image">
            <?php echo $this->Html->image('profile-back.jpg'); ?>
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    <?php echo $this->Html->image('profile-picture.jpg', ['class' => 'avatar border-gray']); ?>
                    <h5 class="title"><?= h($user->name) ?></h5>
                </a>
                <p class="description">
                    <?= __('Email') ?>: <?= h($user->email) ?>
                </p>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><?= __('Pets') ?></h4>
        </div>
        <div class="card-body">
            <ul class="list-unstyled team-members">
                <?php if (!empty($user->pets)): ?>
                    <?php foreach ($user->pets as $pets): ?>
                    <li>
                        <div class="row">                    
                            <div class="col-md-9 col-9">
                                <?= h($pets->name) ?>
                                <br/>
                                <!--span class="text-success">
                                    <small>Hospedado</small>
                                </span-->
                            </div>
                            <div class="col-md-3 col-3 text-right">
                                <?= $this->Html->link('<btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i class="fa fa-info"></i></btn>', ['controller' => 'Pets' ,'action' => 'view', $pets->id], ['escape'=>false]) ?>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label><?= __('Username') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Username') ?>" value="<?= h($user->username) ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label><?= __('Phone') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Phone') ?>" value="<?= h($user->phone) ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><?= __('Mobile') ?></label>
                        <input type="email" class="form-control" disabled="" placeholder="<?= __('Phone') ?>" value="<?= h($user->mobile) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Address Line 1') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Address Line 1') ?>" value="<?= h($user->address_line_1) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Address Line 2') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Address Line 2') ?>" value="<?= h($user->address_line_2) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label><?= __('City') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('City') ?>" value="<?= $user->has('city') ? h($user->city->name) : "" ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label><?= __('Postal Code') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Postal Code') ?>" value="<?= h($user->postal_code) ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label><?= __('Active') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Active') ?>" value="<?= $user->active ? __('Yes') : __('No'); ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit User'), ['controller' => 'Users' ,'action' => 'edit', $user->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Aqui muestro los servicios de la Mascota-->
    <!--div class="col-md-6">
        <div class="card-header">
            <h4 class="card-title"><?= __('Services') ?></h4>
        </div>
        <div class="card-body">
            <ul class="list-unstyled team-members">
            <?php if (!empty($user->pets)): ?>
                    <?php foreach ($user->pets as $services): ?>
                    <li>
                        <div class="row">                    
                            <div class="col-md-9 col-9">
                                <?= h($services->name) ?>
                                <br/>
                                <span class="text-success">
                                    <small>$services->created</small>
                                </span>
                            </div>
                            <div class="col-md-3 col-3 text-right">
                                <?= $this->Html->link('<btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i class="fa fa-info"></i></btn>', ['controller' => 'Services' ,'action' => 'view', $services->id], ['escape'=>false]) ?>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div-->
<!--Hasta aqui-->
<!--
    <div class="related">
        <h4><?= __('Related Emergency Contacts') ?></h4>
        <?php if (!empty($user->emergency_contacts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Phone') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->emergency_contacts as $emergencyContacts): ?>
            <tr>
                <td><?= h($emergencyContacts->id) ?></td>
                <td><?= h($emergencyContacts->name) ?></td>
                <td><?= h($emergencyContacts->phone) ?></td>
                <td><?= h($emergencyContacts->user_id) ?></td>
                <td><?= h($emergencyContacts->created) ?></td>
                <td><?= h($emergencyContacts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'EmergencyContacts', 'action' => 'view', $emergencyContacts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'EmergencyContacts', 'action' => 'edit', $emergencyContacts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'EmergencyContacts', 'action' => 'delete', $emergencyContacts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emergencyContacts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
-->