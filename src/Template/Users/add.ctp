<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

use Cake\Routing\Router;
?>
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-body">
            <?= $this->Form->create($user) ?>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('email'); ?>
                    </div>
                </div>
                <div class="col-md-6 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('username'); ?>
                    </div>
                </div>
                <div class="col-md-6 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('password'); ?>
                    </div>
                </div>
                <div class="col-md-6 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('name'); ?>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('address_line_1'); ?>
                    </div>
                </div>
                <div class="col-md-6 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('address_line_2'); ?>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('country_id', ['options' => $countries, 'id'=>'countries', 'empty' => __('Select a country')]); ?>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('state_id', ['id'=>'states', 'empty' => __('Select a state')]); ?>
                    </div>
                </div>
                <div class="col-md-4 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('city_id', ['id'=>'cities', 'empty' => __('Select a city')]); ?>
                    </div>
                </div>
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('postal_code'); ?>
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('phone'); ?>
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('mobile'); ?>
                    </div>
                </div>
                <div class="col-md-4 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('role_id', ['options' => $roles]); ?>
                    </div>
                </div>
                <div class="col-md-2 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('active'); ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
$("#countries").on('change',function() {
    var id = $(this).val();
    $("#states").find('option').remove();
    if (id) {
        var dataString = 'id='+ id;
        $.ajax({
            dataType:'json',
            type: "POST",
            url: '<?php echo Router::url(array("controller" => "Users", "action" => "get-states")); ?>' ,
            data: dataString,
            cache: false,
            beforeSend: function (xhr) { 
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },  
            success: function(html) {
                $('<option>').val('0').text('<?= __('Select a state') ?>').appendTo($("#states"));
                $.each(html, function(key, value) {              
                    $('<option>').val(value['id']).text(value['name']).appendTo($("#states"));
                });
            } 
        });
    }
});

$("#states").on('change',function() {
    var id = $(this).val();
    $("#cities").find('option').remove();
    if (id) {
        var dataString = 'id='+ id;
        $.ajax({
            dataType:'json',
            type: "POST",
            url: '<?php echo Router::url(array("controller" => "Users", "action" => "get-cities")); ?>' ,
            data: dataString,
            cache: false,
            beforeSend: function (xhr) { 
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            }, 
            success: function(html) {
                $('<option>').val('0').text('<?= __('Select a city') ?>').appendTo($("#cities"));
                $.each(html, function(key, value) {              
                    $('<option>').val(value['id']).text(value['name']).appendTo($("#cities"));
                });
            } 
        });
    }
});
</script>