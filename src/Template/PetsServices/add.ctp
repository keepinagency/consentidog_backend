<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PetsService $petsService
 */
?>

<style>
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
  width: 100%;
}

.autocomplete::-webkit-input-placeholder { /* Edge */
  color: red;
}

.autocomplete:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: red;
}

.autocomplete::placeholder {
  color: red !important;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff;
  border-bottom: 1px solid #d4d4d4;
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9;
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: orangered !important;
  color: #ffffff;
}
</style>
<div class="col-md-3 pr-0">
    <div class="card card-user h-100">
        <div class="image">
            <?php //echo $this->Html->image('profile-back.jpg'); ?>
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    
                    <?php
                    echo $this->Html->image('dog_avatar.png', [
                            'class' => 'avatar border-gray',
                            'id' => 'img_pet', 'name' => 'img_pet'
                        ]); 
                ?>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="card card-user p-0 h-100 pt-4">
        <div class="card-body">
            <?= $this->Form->create($petsService,[
                                        'id'=>'frmservicios',
                                        'autocomplete' => 'off',
                                        'onSubmit' =>'validaform(event);']) ?>
            <div class="row p-0 m-0 pb-3">
                <div class="col-md-12" >
                    <!--div class="col-12 d-flex align-items-center justify-content-center"-->
                        <label class="control-label" for="myPets"><?= __('Pet')?></label><br>
                    <!--/div-->
                    <div class="autocomplete">
                        <input id="myPets" type="text" name="myPets"
                            placeholder="<?= __('Buscar mascota...')?>" 
                            class="form-control">
                    </div>
                </div>
                <input type="hidden" value="" name="pet_id" id="pet-id" class="form-control" required />
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <?php echo $this->Form->control('service_id', ['options' => $services]); ?>
                    </div>
                </div>
            </div>
            
            <!---- 
              CONTENEDOR DE FECHAS 
             ---->
            <div class="row p-0 m-0 " id="conte_fechas">
              <!--input type="hidden" name="confirmed" value="0" /-->
              <div class="row col-11 p-0 m-0 fechas" id="fechas">
                  <div class="col-md-5 pr-1">
                      <div class="form-group">
                          <?php 
                                $date = new DateTime();
                                echo $this->Form->control('masked_start_date', 
                                                          ['name' => 'start_date[]',  //start_date
                                                          'id' => 'start_date[]', 
                                                          'required'=>true,
                                                          'type' => 'datetime-local',
                                                          'class' => 'datetimepicker', 
                                                          'value' => $date->format('Y-m-d\T')."08:00:00" 
                                                          ]); ?>
                      </div>
                  </div>
                  <div class="col-md-5 pr-1">
                      <div class="form-group">
                          <?php echo $this->Form->control('masked_end_date', 
                                                          ['name' => 'end_date[]', //end_date[]
                                                          'required'=>true,
                                                          'id' => 'end_date[]', 
                                                          'type' => 'datetime-local',
                                                          'class' => 'datetimepickerdd', 
                                                          'value' => $date->format('Y-m-d\T')."18:00:00" 
                                                          ]); ?>
                      </div>
                  </div>               
                  <div class="col-md-2 h-100 ">
                    <div class="form-group">
                      <div class="form-group select">
                        <label for="confirmed[]" class="control-label "><?= __('Payed') ?></label><br>    
                        <select name="confirmed[]" id="confirmed[]" class="form-control h-100">
                          <option value="0" selected>NO</option>
                          <option value="1">SI</option>
                        </select>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-1 d-flex align-items-center justify-content-center"
                  id="btn_mas">
                  <input type="button" value="+" onclick="addfecserv();">
              </div>
            </div>




            <div class="row p-0 m-0">
                <div class="update ml-auto mr-auto h-100">
                    <?= $this->Form->button('<i class="fa fa-save fa-fw pr-3"></i>'.__('Save'),[
                                'id'=>'btnsubmit', 'type' => 'Submit',
                                'class' => 'btn btn-primary btn-round']) ?>
                </div>

            </div>
            </fieldset>
            
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    <?php
        echo "var pets = [";
        foreach ($pets as $pet){

            echo "['".$pet->name."',".$pet->id.",'".$pet->photo."','".$pet->photo_dir."'],";

        }
        echo "];";
    ?>

    function validaform(e){
        
        pet = document.getElementById("pet-id").value;
        if (pet==""){
            e.preventDefault();
            //myPets = document.getElementById("myPets").value;
            $('#myPets').css('border-color','orangered');
            $('#myPets').focus();
            return false;
        }
    }

    function addfecserv(){
      $("#fechas").clone(true).appendTo("#conte_fechas");
      $("#btn_mas").clone(true).appendTo("#conte_fechas");
      
      let btnmenos= '&nbsp;<input type="button" value=" -" onclick="remfecserv(this);">';
      $("#conte_fechas div:last").append(btnmenos);
      //$("body #fechas:last").find("input:hidden").remove();
      //css("background-color","red");
      //
    }

    function remfecserv(e){
      //$(e).parent().prev().css('background-color','red');
      //$(e).parent().css('background-color','blue');
      $(e).parent().prev().remove();
      $(e).parent().remove();
    }


    function muestrafoto2(photo,photo_dir){
        //let photo = $("#pet-id").find(':selected').attr('data-photo');
        //let photo_dir = $("#pet-id").find(':selected').attr('data-photo_dir');
        
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        photo_pet = baseUrl + '/files/pets/photo/' + photo_dir + '/' + photo;
        if ( photo == "" ){
            photo_pet = baseUrl + '/img/dog_avatar.png';
        }
        $('#img_pet').attr('src',photo_pet);
    }


    $(document).ready(function() {   
      
      /*
      */
    });

/// código para autocomplete

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        //nombre de la mascota
        nompet = arr[i][0]; 
        if (nompet.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + nompet.substr(0, val.length) + "</strong>";
          b.innerHTML += nompet.substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + nompet + "'>";
          b.innerHTML += "<input type='hidden' value='" + arr[i][1] + "'>";
          b.innerHTML += "<input type='hidden' value='" + arr[i][2] + "'>";
          b.innerHTML += "<input type='hidden' value='" + arr[i][3] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              inppet_id = document.getElementById("pet-id");  
              inppet_id.value = this.getElementsByTagName("input")[1].value;
              muestrafoto2(this.getElementsByTagName("input")[2].value,this.getElementsByTagName("input")[3].value);
              $('#myPets').css('border-color','');
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      document.getElementById("pet-id").value='';
      $('#myPets').css('border-color','');
      //dog_avatar.png
      muestrafoto2("","");
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
    closeAllLists(e.target);
});
}


autocomplete(document.getElementById("myPets"), pets);


</script>