<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PetsService[]|\Cake\Collection\CollectionInterface $petsServices
 */


?>
<div class="col-12 text-right d-flex flex-row row">
    <div class="col-9">
        <form method="get" accept-charset="utf-8" role="form" action="pets-services" id="frmbuscar"
                    class="d-flex flex-row">
            <div class="input-group no-border col-9">
                <input name="q" id="q" type="text" 
                    value="<?= $q?>" class="form-control" placeholder="Buscar..."
                    onKeyUp="if (event.keyCode === 13) {
                                    document.getElementById('frmbuscar').submit();
                                }">
                    
                <select id="consulta" name="consulta" class="form-control ml-1 p-0" class="form-control" 
                    onKeyUp="if (event.keyCode === 13) 
                        {document.getElementById('frmbuscar').submit();}"
                    onChange ="document.getElementById('frmbuscar').submit();">
                    <?php
                        $todossel = '';
                        if ($consulta == '*'){
                            $todossel = 'selected';
                        }
                    ?>
                    <option value="0" <?= $todossel;?>>Todos</option>
                    <?php foreach ($services as $service): ?>
                        <?php
                            $optsel = '';
                            if ($consulta == $service->id){
                                $optsel = 'selected';
                            }
                        ?>
                        <option value="<?= $service->id; ?>" <?= $optsel;?>>
                            <?= $service->name; ?>
                        </option>
                    <?php endforeach; ?>    
                </select>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="nc-icon nc-zoom-split" 
                            onClick="document.getElementById('frmbuscar').submit();"></i>
                    </div>
                </div>
            </div>
            <!--div class="border border-danger col-4 text-left"-->
            <div class="input-group no-border col-3 m-auto">
                <label class="d-flex flex-row ">
                    <?php
                        $checked = "";
                        if ($pagados>0){
                            $checked = "checked";
                        }
                    ?>
                    <input name="pagados" id="pagados" type="checkbox" 
                        value="1" class="form-controccl mt-1 mr-1" <?= $checked;?>
                        onClick="document.getElementById('frmbuscar').submit();">No pagos
                </label>
            </div>
        </form>
    </div>

    <div class="col-3 text-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add Pet Service'), ['controller' => 'PetsServices' ,'action' => 'add'], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
    </div>
</div>
  
<div class="col-md-12">
    <div class="card">
        <div class="card-body ">
            <div class="table-responsive p-0 m-0">
                <table class="table p-0 m-0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('pet_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('service_id') ?></th>
                            <!--th scope="col"><?= $this->Paginator->sort('amount') ?></th-->
                            <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('confirmed',__("Payed")) ?></th>
                            <th scope="col" class="p-0 m-0"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($petsServices as $petsService): ?>
                        <tr>
                            <td><?= $this->Number->format($petsService->id) ?></td>
                            <td><?= $petsService->has('pet') ? $this->Html->link($petsService->pet->name, ['controller' => 'Pets', 'action' => 'view', $petsService->pet->id]) : '' ?></td>
                            <td><?= $petsService->has('service') ? $this->Html->link($petsService->service->name, ['controller' => 'Services', 'action' => 'view', $petsService->service->id]) : '' ?></td>
                            <!--td><?php // $petsService->has('employee') ? $this->Html->link($petsService->employee->name, ['controller' => 'Employees', 'action' => 'view', $petsService->employee->id]) : '' ?></td-->
                            
                            <!--td><?= h($petsService->movements_id->amout) ?></td-->
                            <td><?= h($petsService->start_date) ?></td>
                            <td><?= h($petsService->end_date) ?></td>
                            <td><?= $petsService->confirmed ? __('Yes') : __('No'); ?></td>
                            <td class="actions p-0 m-0">
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-info-circle"></i>', 
                                        ['action' => 'view', $petsService->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-edit"></i>', 
                                        ['action' => 'edit', $petsService->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="btnact fas fa-trash"></i>', 
                                        ['action' => 'delete', $petsService->id], ['escape' => false , 
                                            'confirm' => __('Are you sure you want to delete # {0}?', $petsService->service->name)])?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>