<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PetsService $petsService
 */
?>
<div class="col-md-3 pr-0">
    <div class="card card-user h-100">
        <div class="image">
            <?php //echo $this->Html->image('profile-back.jpg'); ?>
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    
                    <?php
                    if (empty($pet->photo)){
                        echo $this->Html->image('dog_avatar.png', [
                                'class' => 'avatar border-gray',
                                'id' => 'img_pet', 'name' => 'img_pet'
                            ]); 
                    }else{
                        echo $this->Html->image('../files/pets/photo/'.$pet->photo_dir. '/'.$pet->photo, [
                            'class' => 'avatar border-gray',
                            'id' => 'img_pet', 'name' => 'img_pet'
                        ]); 
                    }
                    ?>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="card card-user p-0 h-100 pt-4">
        <div class="card-body">
            <?= $this->Form->create($petsService,['id'=>'frmservicios']) ?>
            <div class="row p-0 m-0">
                <div class="col-md-12">
                    <div class="form-group">
                    <label class="control-label" for="pet-id"><?= __('Pet')?></label>
                        <select name="pet_id" id="pet-id" class="form-control" onchange="muestrafoto();">
                            <option value=""><?= __('Seleccione...')?></option>
                            <?php 
                            foreach ($pets as $pet): ?>
                                <option value="<?= $pet->id?>"
                                    <?php
                                        if ($petsService->pet_id ==$pet->id){
                                            echo "selected = 'SELECTED'";
                                        }
                                    ?>  data-photo="<?= $pet->photo?>"
                                        data-photo_dir="<?= $pet->photo_dir?>"><?= $pet->name?></option>
                                <?php 
                            endforeach; 
                            ?>
                        </select>
                        <?php //echo $this->Form->control('pet_id', ['options' => $pets]); ?>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <?php echo $this->Form->control('service_id', ['options' => $services]); ?>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-5 pr-2">
                    <div class="form-group">
                        <?php echo $this->Form->control('masked_start_date', 
                                                        ['name' => 'start_date', 
                                                        'id' => 'datetimepickerstart', 
                                                        'class' => 'datetimepicker', 
                                                        'placeholder' => $petsService->start_date,
                                                        'value' => $petsService->start_date]); ?>
                    </div>
                </div>
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <?php echo $this->Form->control('masked_end_date', 
                                                        ['name' => 'end_date',
                                                        'id' => 'datetimepickerend', 
                                                        'class' => 'datetimepicker', 
                                                        'placeholder' => $petsService->end_date,
                                                        'value' => $petsService->end_date]); ?>
                    </div>
                </div>
                <div class="col-md-2 h-100 ">
                    
                    <div class="col-12 p-0 m-0">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <label><?= __('Payed') ?></label><br>    
                        </div>
                        <div class="col-12  d-flex align-items-center justify-content-center">
                            <?php 
                            echo $this->Form->control('confirmed',[
                                                        'label' => '',
                                                        'style' => 'transform: scale(1.5); height:40px;',
                                                        'class' => 'pl-5'
                                                    ]); 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="update ml-auto mr-auto h-100">
                    <?= $this->Form->button('<i class="fa fa-refresh fa-fw pr-3"></i>'.__('Refresh service'),[
                                    'id'=>'btnsubmit', 'type' => 'Submit',
                                    'class' => 'btn btn-primary btn-round']) ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
    function muestrafoto(){
        let photo = $("#pet-id").find(':selected').attr('data-photo');
        let photo_dir = $("#pet-id").find(':selected').attr('data-photo_dir');
        
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        photo_pet = baseUrl + '/files/pets/photo/' + photo_dir + '/' + photo;
        if ( photo == "" ){
            photo_pet = baseUrl + '/img/dog_avatar.png';
        }
        $('#img_pet').attr('src',photo_pet);
    }
    
    $(document).ready(function() {
        if ($("#datetimepickerstart").length != 0) {
          $('#datetimepickerstart').datetimepicker({ 
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fas fa-trash',
              close: 'fa fa-remove'
            },
            format: "DD/MM/Y hh:mm A"
          });
        }
        if ($("#datetimepickerend").length != 0) {
          $('#datetimepickerend').datetimepicker({ 
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fas fa-trash',
              close: 'fa fa-remove'
            },
            format: "DD/MM/Y hh:mm A"
          });
        }
    });
    document.getElementById('btnsubmit').onclick = function() {
      var confirm = document.getElementById('confirmed').checked;
      var execute = document.getElementById('executed').checked;
        if ((execute) && (!(confirm))){
            alert('Por favor, debe confirmar la reserva...');
            return false;
        }else {
            //alert('Selecionado correctamente');
            return true;
        }
    };
</script>
