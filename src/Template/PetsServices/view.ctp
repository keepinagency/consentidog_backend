<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PetsService $petsService
 */
?>
<div class="col-md-3 pr-0">
    <div class="card card-user h-100">
        <div class="image">
            <?php //echo $this->Html->image('profile-back.jpg'); ?>
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    <?php 
                        //
                    if (empty($petsService->pet->photo_dir)){
                        echo $this->Html->image('dog_avatar.png', ['class' => 'avatar border-gray']); 
                    }else{    
                        ?>
                        <img 
                            src="<?php echo '../../files/pets/photo/'.$petsService->pet->photo_dir. '/'.$petsService->pet->photo;?>" 
                            class="avatar border-gray"/>
                    <?php
                    }
                    ?>
                    <h5 class="title"><?= h($petsService->pet->name) ?></h5>
                    
                </a>
                <p class="description">
                    <?= __('User') ?>: <?= $petsService->pet->has('user') ? $this->Html->link($petsService->pet->user->name, ['controller' => 'Users', 'action' => 'view', $petsService->pet->user->id]) : '' ?>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9 ">
    <div class="card card-user p-0 h-100 pt-4">
        <div class="card-body">
            <div class="row p-0 m-0">
                <div class="col-md-2">
                    <div class="form-group p-0 m-0">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" 
                               disabled="" placeholder="<?= __('Id') ?>" 
                               value="<?= h($petsService->id) ?>">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label><?= __('Pet') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Pet') ?>" value="<?= h($petsService->pet->name) ?>">
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label><?= __('Service') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Service') ?>" value="<?= h($petsService->service->name) ?>">
                    </div>
                </div>
                <!--div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label><?= __('Responsable') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Employee') ?>" value="<?= h($petsService->employee->name) ?>">
                    </div>
                </div-->
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-5">
                    <div class="form-group">
                        <label><?= __('Start Date') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Start Date') ?>" value="<?= h($petsService->start_date) ?>">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label><?= __('End Date') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('End Date') ?>" value="<?= h($petsService->end_date) ?>">
                    </div>
                </div>
                <div class="col-md-2 ">
                    <div class="form-group">
                        <label><?= __('Payed') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Confirmed') ?>" value="<?= $petsService->confirmed ? __('Yes') : __('No'); ?>">
                    </div>
                </div>
                <!--div class="col-md-1 pl-1">
                    <div class="form-group">
                        <label><?= __('Executed') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Executed') ?>" value="<?= $petsService->executed ? __('Yes') : __('No'); ?>">
                    </div>
                </div-->
            </div>
            <div class="row h-25 pt-4">
                <div class="update ml-auto mr-auto h-100">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit Pet Service'), ['controller' => 'PetsServices' ,'action' => 'edit', $petsService->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>