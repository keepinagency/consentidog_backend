<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pet[]|\Cake\Collection\CollectionInterface $pets
 */
?>
<div class="col-12 text-right d-flex flex-row row">
    <div class="col-6 text-left">
        <form method="get" accept-charset="utf-8" role="form" action="pets" id="frmbuscar">
            <div class="input-group no-border">
                <input name="q" id="q" type="text" 
                    value="" class="form-control" placeholder="Buscar..."
                    onKeyUp="if (event.keyCode === 13) {
                                    document.getElementById('frmbuscar').submit();
                                }">
            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split" 
                        onClick="document.getElementById('frmbuscar').submit();"></i>
                </div>
            </div>
            </div>
        </form>
    </div>
    <div class="col-6 text-right">
        <?= $this->Html->link('<i class="fa fa-plus-circle fa-fw"></i> '.__('Add Pet'), ['controller' => 'Pets' ,'action' => 'add'], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
    </div>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">&nbsp;</th>
                            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                            <!--th scope="col"><?= $this->Paginator->sort('gender_id') ?></th-->
                            <th scope="col"><?= $this->Paginator->sort('sterile') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('birth_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('colour_id') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pets as $pet): ?>
                        <tr>
                            <td>
                                <?php
                                if (empty($pet->photo_dir)){
                                    echo $this->Html->image('dog_avatar.png', [
                                        'class' => 'avatar border-gray',
                                        'id' => 'img_pet', 'name' => 'img_pet'
                                    ]); 
                                }else{
                                    ?>
                                    <img 
                                        src="<?php echo './files/pets/photo/'.$pet->photo_dir. '/'.$pet->photo;?>" 
                                        class="avatar border-gray"/>
                                    <?php
                                }
                                ?>
                                <?php //echo '../files/pets/photo/'.$pet->photo_dir. '/'.$pet->photo;?>
                            </td>
                            <td><?= h($pet->name) ?></td>
                            <!--td><?= $pet->has('gender') ? $this->Html->link($pet->gender->display_name, ['controller' => 'Genders', 'action' => 'view', $pet->gender->id]) : '' ?></td-->
                            <td><?= $pet->sterile ? __('Yes') : __('No'); ?></td>
                            <td><?= h($pet->birth_date) ?></td>
                            <td><?= $this->Number->format($pet->weight) ?></td>
                            <td><?= $pet->has('user') ? $this->Html->link($pet->user->name, ['controller' => 'Users', 'action' => 'view', $pet->user->id]) : '' ?></td>
                            <td><?= $pet->has('colour') ? $this->Html->link($pet->colour->name, ['controller' => 'Colours', 'action' => 'view', $pet->colour->id]) : '' ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-info-circle"></i>', ['action' => 'view', $pet->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="btnact pr-2 fas fa-edit"></i>', ['action' => 'edit', $pet->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<i class="btnact fas fa-trash"></i>', ['action' => 'delete', $pet->id], ['escape' => false , 'confirm' => __('Are you sure you want to delete # {0}?', $pet->name)])?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
