<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pets $pet
 */
$breeds_text = null;
?>
<div class="col-md-4">
    <div class="card card-user h-100">
        <div class="image">
            <?php //echo $this->Html->image('profile-back.jpg'); ?>
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    <?php 
                        //
                    if (empty($pet->photo_dir)){
                        echo $this->Html->image('dog_avatar.png', ['class' => 'avatar border-gray']); 
                    }else{    
                        ?>
                        <img 
                            src="<?php echo '../../files/pets/photo/'.$pet->photo_dir. '/'.$pet->photo;?>" 
                            class="avatar border-gray"/>
                    <?php
                    }
                    ?>
                    <h5 class="title"><?= h($pet->name) ?></h5>
                    
                </a>
                <p class="description">
                    <?= __('User') ?>: <?= $pet->has('user') ? $this->Html->link($pet->user->name, ['controller' => 'Users', 'action' => 'view', $pet->user->id]) : '' ?>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card card-user p-0 m-0 h-100">
        <div class="card-body">
            <div class="row p-0 m-0 pt-3">
                <div class="col-md-4 ">
                    <div class="form-group">
                        <label><?= __('Colour') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Colour') ?>" value="<?= h($pet->colour->name) ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><?= __('Weight') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Weight') ?>" value="<?= $this->Number->format($pet->weight) ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><?= __('Birth Date') ?></label>
                        <input type="email" class="form-control" disabled="" placeholder="<?= __('Birth Date') ?>" value="<?= h($pet->birth_date) ?>">
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><?= __('Additional Information') ?></label>
                        <textarea disabled="" class="form-control textarea"><?= h($pet->additional_information); ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0">
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?= __('Gender') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Gender') ?>" value="<?= $pet->has('gender') ? $pet->gender->display_name : '' ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label><?= __('Sterile') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Sterile') ?>" value="<?= $pet->sterile ? __('Yes') : __('No'); ?>">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <?php 
                        if (!empty($pet->breeds)): 
                            foreach ($pet->breeds as $breeds):
                                $breeds_text == null ? $breeds_text = $breeds->name : $breeds_text = $breeds_text . ", " . $breeds->name;
                            endforeach;
                        else:
                            $breeds_text = __('Not Specified');
                        endif; 
                        ?>
                        <label><?= __('Breed') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Breed') ?>" value="<?= $breeds_text ?>">
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0 ">
                <div class="update mx-auto h-100 pt-2">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit Pet'), ['controller' => 'Pets' ,'action' => 'edit', $pet->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Aqui muestro los servicios de la Mascota-->
<div class="col-12 pt-2">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><?= __('Services') ?></h4>
        </div>
        <div class="card-body">
            <div class="table-responsive p-0 m-0">
                <table class="table p-0 m-0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('service_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('confirmed',__("Payed")) ?></th>
                            <th scope="col" class="p-0 m-0"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pet->pets_services as $petsService): ?>
                        <tr>
                            <td><?= $this->Number->format($petsService->id) ?></td>
                            <td><?= $petsService->has('service') ? $this->Html->link($petsService->service->name, ['controller' => 'Services', 'action' => 'view', $petsService->service->id]) : '' ?></td>
                            <!--td><?php // $petsService->has('employee') ? $this->Html->link($petsService->employee->name, ['controller' => 'Employees', 'action' => 'view', $petsService->employee->id]) : '' ?></td-->
                            
                            <!--td><?= h($petsService->movements_id->amout) ?></td-->
                            <td><?= h($petsService->start_date) ?></td>
                            <td><?= h($petsService->end_date) ?></td>
                            <td><?= $petsService->confirmed ? __('Yes') : __('No'); ?></td>
                            <td class="actions p-0 m-0">
                                <?= $this->Html->link('<i class="btnact fas fa-info-circle"></i>', 
                                        ['controller' => 'PetsServices', 'action' => 'view', $petsService->id], ['escape' => false]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Hasta aqui-->