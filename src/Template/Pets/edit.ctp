<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pet $pet
 */
?>
<div class="col-md-4 p-0 m-0">
  <div class="card card-user">
    <div class="card-body">
      <?= $this->Form->create($pet, ['type' => 'file']) ?>
      <!--fieldset-->
      <div class="col-md-12 pr-1">
        <div class="form-group  d-flex justify-content-center p-0 m-0">
          <img src="<?php echo '../../files/pets/photo/'.$pet->photo_dir. '/'.$pet->photo;?>" class="avatar border-gray"/>
        </div>
        <div class="form-group">
          <?php echo $this->Form->control('photo', [
              'type' => 'file', 'class' => 'filestyle', 
              'data-btnClass' => 'btn-secundary',
              'data-text' => 'Subir Foto', /*'htmlIcon' => '<span class=\'fas fa-folder-open\'></span>',*/
              'data-size' => 'sm', 'data-placeholder' => 'Actualizar', 'id' => 'seleccionArchivos'
            ]); 
          ?>
        </div>
        <div class="form-group">
            <?php echo $this->Form->control('name'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-8 ">
    <div class="card card-user">
        <div class="card-body">
            <?= $this->Form->create($pet, ['type' => 'file']) ?>
            <!--fieldset-->
              <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php echo $this->Form->control('gender_id', ['options' => $genders]); ?>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <?php echo $this->Form->control('masked_birth_date', ['id' => 'datetimepicker', 'class' => 'datetimepicker', 'placeholder' => $pet->birth_date]); ?>
                    </div>
                  </div>
                  <div class="col-md-2 pr-2 pt-4">
                      <div class="form-group">
                          <?php echo $this->Form->control('sterile'); ?>
                      </div>
                  </div>
                  <div class="col-md-2 pr-2">
                    <div class="form-group">
                      <?php echo $this->Form->control('weight'); ?>
                    </div>
                  </div>
                  <div class="col-md-12 pr-2">
                    <div class="form-group">
                      <?php echo $this->Form->control('additional_information'); ?>
                    </div>
                  </div>
                  <div class="col-md-3 pr-2">
                    <div class="form-group">
                      <?php echo $this->Form->control('user_id', ['options' => $users]); ?>
                    </div>
                  </div>
                  <div class="col-md-3 pr-2">
                    <div class="form-group">
                      <?php echo $this->Form->control('colour_id', ['options' => $colours]); ?>
                    </div>
                  </div>
                  <div class="col-md-6 pr-4">
                    <div class="form-group">
                      <?php echo $this->Form->control('breeds._ids', ['options' => $breeds]); ?>
                    </div>
                  </div>
              </div>
            <!--/fieldset-->
            <!--?= $this->Form->button(__('Submit')) ?-->
            <!--?= $this->Form->end() ?-->
        </div>
        <!--?= $this->Form->button(__('Submit')) ?-->
            <!--?= $this->Form->end() ?-->
    </div>
    <div class="text-right">
      <?= $this->Form->button(__('Submit')) ?>
      <?= $this->Form->end() ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        if ($("#datetimepicker").length != 0) {
          $('#datetimepicker').datetimepicker({
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fas fa-trash',
              close: 'fa fa-remove'
            }
          });
        }
    });
</script>