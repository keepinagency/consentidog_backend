<?php
/**
 * @var \App\View\AppView $this
 * 
 */
use Cake\Routing\Router;
?>

<div class="col-md-12">
    <div class="card card-user">
        <div class="card-body">
            <?= $this->Form->create($pet, ['type' => 'file']) ?>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('name'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('gender_id', ['options' => $genders]); ?>
                        </div>
                    </div>
                    <div class="col-md-2 pr-1 pt-4">
                        <div class="form-group">
                            <?php echo $this->Form->control('sterile'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('masked_birth_date', ['id' => 'datetimepicker', 'class' => 'datetimepicker']); ?>
                        </div>
                    </div>
                    <div class="col-md-2 pr-2">
                        <div class="form-group">
                            <?php echo $this->Form->control('weight'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 p-0">
                    <!--i class="fas fa-folder-open"></i-->
                        <div class="form-group">
                            <?php echo $this->Form->control('photo', ['type' => 'file',
                                                                      'class' => 'filestyle',
                                                                      'data-btnClass' => 'btn-secundary',
                                                                      'data-text' => 'Subir Foto',
                                                                      'data-size' => 'sm',
                                                                      'data-placeholder' => 'Seleccione...',
                                                                      'id' => 'seleccionArchivos']); ?>
                        </div> 
                    </div>
                    <div class="col-md-2 pr-2">
                        <div class="form-group">
                            <img id="imagenPrevisualizacion" class="avatar">
                        </div>
                    </div>
                    <div class="col-md-12 pr-2">
                        <div class="form-group">
                            <?php echo $this->Form->control('additional_information'); ?>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('user_id', ['options' => $users, 'id'=>'user']); ?>
                        </div>
                    </div>
                    <!--div class="col-md-6 pr-1">
                        <div class="form-group">
                            <div>
                                <label>Agregar usuario para la mascota</label>
                            </div>
                                <button 
                                    type="button" class="btn btn-primary btn-sm" 
                                    data-toggle="modal" 
                                    data-target=".bd-example-modal-lg"
                                    data-remote="http://localhost:8080/consentidog_backend/users/add">
                                        <i class="fa fa-plus-circle">
                                            <a href="http://localhost:8080/consentidog_backend/users/add"></a->
                                        </i>
                                </button>
                        </div>
                    </div-->
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('colour_id', ['options' => $colours, 'id'=>'color']); ?>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <?php echo $this->Form->control('breeds._ids', ['options' => $breeds, 'id'=>'breed']); ?>
                        </div>
                    </div>
                </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<!-- overlayed element >
<div id="dialogModal">
     the external content is loaded inside this tag
     <div class="contentWrap"></div>
   </div>
<div class="actions">
    <ul>
        <li>
            <?= $this->Html->link(__('Add user', true), 
                array("controller"=>"users", "action"=>"add"), 
                array("class"=>"overlay", "title"=>"Add User"));
            ?>
        </li>
    </ul>
</div-->
<script>
    
    /*function ordenarSelect(name){
        var selectToSort = jQuery('#' + name);
        var optionActual = selectToSort.val();
        selectToSort.html(selectToSort.children('option').sort(function (a, b) {
                return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
            })).val(optionActual);
    }
    $(document).ready(function () {
      ordenarSelect('breed');
    });*/

    $(document).ready(function() {
        if ($("#datetimepicker").length != 0) {
          $('#datetimepicker').datetimepicker({ 
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fas fa-trash',
              close: 'fa fa-remove'
            }
          });
        }


        $("#seleccionArchivos").onchange = function() {
            // If file size > 500kB, resize such that width <= 1000, quality = 0.9
            reduceFileSize(this.files[0], 500*1024, 1000, Infinity, 0.9, blob => {
                let body = new FormData();
                body.set('file', blob, blob.name || "file.jpg");
                fetch('/upload-image', {method: 'POST', body}).then(alert('comprimida'));
            });
        };
    });

        // Obtener referencia al input y a la imagen
        const $seleccionArchivos = document.querySelector("#seleccionArchivos"),
        $imagenPrevisualizacion = document.querySelector("#imagenPrevisualizacion");

        // Escuchar cuando cambie
        $seleccionArchivos.addEventListener("change", () => {
            // Los archivos seleccionados, pueden ser muchos o uno
            const archivos = $seleccionArchivos.files;
            // Si no hay archivos salimos de la función y quitamos la imagen
            if (!archivos || !archivos.length) {
                $imagenPrevisualizacion.src = "";
                return;
            }
            // Ahora tomamos el primer archivo, el cual vamos a previsualizar
            const primerArchivo = archivos[0];
            // Lo convertimos a un objeto de tipo objectURL
            const objectURL = URL.createObjectURL(primerArchivo);
            // Y a la fuente de la imagen le ponemos el objectURL
            $imagenPrevisualizacion.src = objectURL;

            reduceFileSize(this.files[0], 500*1024, 1000, Infinity, 0.9, blob => {
                let body = new FormData();
                body.set('file', blob, blob.name || "file.jpg");
                fetch('/upload-image', {method: 'POST', body}).then(alert('comprimida'));
            });
        });
</script>


<script>
// From https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toBlob, needed for Safari:
if (!HTMLCanvasElement.prototype.toBlob) {
    Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
        value: function(callback, type, quality) {

            var binStr = atob(this.toDataURL(type, quality).split(',')[1]),
                len = binStr.length,
                arr = new Uint8Array(len);

            for (var i = 0; i < len; i++) {
                arr[i] = binStr.charCodeAt(i);
            }

            callback(new Blob([arr], {type: type || 'image/png'}));
        }
    });
}

window.URL = window.URL || window.webkitURL;

// Modified from https://stackoverflow.com/a/32490603, cc by-sa 3.0
// -2 = not jpeg, -1 = no data, 1..8 = orientations
function getExifOrientation(file, callback) {
    // Suggestion from http://code.flickr.net/2012/06/01/parsing-exif-client-side-using-javascript-2/:
    if (file.slice) {
        file = file.slice(0, 131072);
    } else if (file.webkitSlice) {
        file = file.webkitSlice(0, 131072);
    }

    var reader = new FileReader();
    reader.onload = function(e) {
        var view = new DataView(e.target.result);
        if (view.getUint16(0, false) != 0xFFD8) {
            callback(-2);
            return;
        }
        var length = view.byteLength, offset = 2;
        while (offset < length) {
            var marker = view.getUint16(offset, false);
            offset += 2;
            if (marker == 0xFFE1) {
                if (view.getUint32(offset += 2, false) != 0x45786966) {
                    callback(-1);
                    return;
                }
                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;
                for (var i = 0; i < tags; i++)
                    if (view.getUint16(offset + (i * 12), little) == 0x0112) {
                        callback(view.getUint16(offset + (i * 12) + 8, little));
                        return;
                    }
            }
            else if ((marker & 0xFF00) != 0xFF00) break;
            else offset += view.getUint16(offset, false);
        }
        callback(-1);
    };
    reader.readAsArrayBuffer(file);
}

// Derived from https://stackoverflow.com/a/40867559, cc by-sa
function imgToCanvasWithOrientation(img, rawWidth, rawHeight, orientation) {
    var canvas = document.createElement('canvas');
    if (orientation > 4) {
        canvas.width = rawHeight;
        canvas.height = rawWidth;
    } else {
        canvas.width = rawWidth;
        canvas.height = rawHeight;
    }

    if (orientation > 1) {
        console.log("EXIF orientation = " + orientation + ", rotating picture");
    }

    var ctx = canvas.getContext('2d');
    switch (orientation) {
        case 2: ctx.transform(-1, 0, 0, 1, rawWidth, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, rawWidth, rawHeight); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, rawHeight); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, rawHeight, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, rawHeight, rawWidth); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, rawWidth); break;
    }
    ctx.drawImage(img, 0, 0, rawWidth, rawHeight);
    return canvas;
}

function reduceFileSize(file, acceptFileSize, maxWidth, maxHeight, quality, callback) {
    if (file.size <= acceptFileSize) {
        callback(file);
        return;
    }
    var img = new Image();
    img.onerror = function() {
        URL.revokeObjectURL(this.src);
        callback(file);
    };
    img.onload = function() {
        URL.revokeObjectURL(this.src);
        getExifOrientation(file, function(orientation) {
            var w = img.width, h = img.height;
            var scale = (orientation > 4 ?
                Math.min(maxHeight / w, maxWidth / h, 1) :
                Math.min(maxWidth / w, maxHeight / h, 1));
            h = Math.round(h * scale);
            w = Math.round(w * scale);

            var canvas = imgToCanvasWithOrientation(img, w, h, orientation);
            canvas.toBlob(function(blob) {
                console.log("Resized image to " + w + "x" + h + ", " + (blob.size >> 10) + "kB");
                callback(blob);
            }, 'image/jpeg', quality);
        });
    };
    img.src = URL.createObjectURL(file);
}
</script>