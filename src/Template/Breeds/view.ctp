<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Breed $breed
 */
?>
<div class="col-md-8">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Name') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($breed->name) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($breed->id) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($breed->created) ?>">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($breed->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Edit Breed'), ['controller' => 'Breeds' ,'action' => 'edit', $breed->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
            <h4><?= __('Related Pets') ?></h4>
            <div class="row related">
                <?php if (!empty($breed->pets)): ?>
                <?php foreach ($breed->pets as $pets): ?>
                <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= h($pets->id) ?>">
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label><?= __('Name') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Name') ?>" value="<?= h($pets->name) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Gender') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Gender') ?>" value="<?= h($pets->gender) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Sterile') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Sterile') ?>" value="<?= h($pets->sterile) ?>">
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label><?= __('Birth Date') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Birth Date') ?>" value="<?= h($pets->birth_date) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Weight') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Weight') ?>" value="<?= h($pets->weight) ?>">
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label><?= __('Additional Information') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Additional Information') ?>" value="<?= h($pets->additional_information) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('User Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('User Id') ?>" value="<?= h($pets->user_id) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="form-group">
                            <label><?= __('Colour Id') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Colour Id') ?>" value="<?= h($pets->colour_id) ?>">
                        </div>
                    </div>
                    <div class="col-md-3 pr-1">
                        <div class="form-group">
                            <label><?= __('Created') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($pets->created) ?>">
                        </div>
                    </div>
                    <div class="col-md-3 pr-1">
                        <div class="form-group">
                            <label><?= __('Modified') ?></label>
                            <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($pets->modified) ?>">
                        </div>
                    </div>
                    <div class="col-md-2 pr-1">
                        <div class="actions">
                            <label><?= __('Actions') ?></label>
                        </div>
                        <div>
                            <?= $this->Html->link('<i class="fa fa-info-circle"></i>', ['controller' => 'Pets', 'action' => 'view', $pets->id], ['escape' => false]) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Pets', 'action' => 'edit', $pets->id], ['escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fas fa-trash"></i>', ['controller' => 'Pets', 'action' => 'delete', $pets->id], ['escape' => false], ['confirm' => __('Are you sure you want to delete # {0}?', $pets->id)]) ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
