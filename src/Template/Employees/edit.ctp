<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<div class="col-md-8">
    <div class="card card-user">
        <div class="card-body">
            <?= $this->Form->create($employee) ?>
            <fieldset>
                <?php
                    echo $this->Form->control('name', ['autocomplete' => 'off'] );
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
