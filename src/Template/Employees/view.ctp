<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Employee $employee
 */
?>
<div class="col-md-12">
    <div class="card card-user">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 pr-2">
                    <div class="form-group">
                        <label><?= __('Empleado') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Employee') ?>" value="<?= h($employee->name) ?>">
                    </div>
                </div>
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                        <label><?= __('Id') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Id') ?>" value="<?= $this->Number->format($employee->id) ?>">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label><?= __('Created') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Created') ?>" value="<?= h($employee->created) ?>">
                    </div>
                </div>
                <div class="col-md-4 pr-2">
                    <div class="form-group">
                        <label><?= __('Modified') ?></label>
                        <input type="text" class="form-control" disabled="" placeholder="<?= __('Modified') ?>" value="<?= h($employee->modified) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <?= $this->Html->link('<i class="fa fa-edit fa-fw"></i> '.__('Modificar Empleado'), ['controller' => 'Employees' ,'action' => 'edit', $employee->id], ['class'=>'btn btn-primary btn-round', 'escape'=>false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>


