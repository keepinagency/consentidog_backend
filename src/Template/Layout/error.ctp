<?php
use Cake\Core\Configure;
?>
<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <?= $this->Html->meta('icon') ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Consentidog - Para tu mejor amigo, el mejor lugar
    <?php $this->assign('title', $title); ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <!-- CSS Files -->
  <?= $this->Html->css('bootstrap.min.css') ?>
  <?= $this->Html->css('paper-dashboard.css') ?>
  <?= $this->Html->css('paper-kit-lite.css') ?>
  <?= $this->Html->css('all.css') ?>
  <?= $this->Html->css('keepincustom.css') ?>

    
  <!--   Core JS Files   -->
  <?= $this->Html->script(['jquery.min.js','popper.min.js', 'bootstrap.min.js']) ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>

  <script src="https://kit.fontawesome.com/2f4013e4ee.js" crossorigin="anonymous"></script>


  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
</head>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
        <a class="simple-text logo-mini" href="" >
          <div class="logo-image-small">
            <?php echo $this->Html->image('logo-small.png', ['alt' => 'Consentidog'], ['controller' => 'Users' ,'action' => 'dashboard']); ?>
          </div>
        </a>
        <div class="simple-text logo-normal">
            <?php echo $this->Html->link(__('Consentidog'), ['controller' => 'Users' ,'action' => 'dashboard'])?>
        </div>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li <?php echo ($this->request->getParam('controller') == 'Users' && $this->request->getParam('action') == 'dashboard') ? "class='active'" : "" ?>>
            <?= $this->Html->link('<i class="nc-icon nc-shop"></i><p>'.__('Home').'</p>', ['controller' => 'Users' ,'action' => 'dashboard'], ['escape' => false]) ?>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel h-100">
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="#title"><?= __('Error no controlado') ?></a>
                </div>
            </div>
        </nav>
        <div class="content pl-3 pr-3">
            <div class="card card-user p-0 m-0 " 
                style="background:url('<?= $this->Url->image('bg_saddog.jpg');?>') top no-repeat;
                       background-size: cover; 
                       height:80vh;">
                <div class="card-body" 
                     >
                    <div class="p-0 m-0 pt-3">
                        <div class="alert alert-danger alert-dismissible fade show">
                          <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="nc-icon nc-simple-remove"></i>
                          </button>
                          <span>
                            <b>Disculpe las molestias ocasionadas </b><br>
                            esto es un mensaje de algún error que no manejamos
                          </span>
                        </div>
                        <div class="alert alert-warning">
                            <?= $this->fetch('content') ?>
                        </div>
                        <div class="text-center">
                            <span>
                            <b>Por favor realice una captura de pantalla</b><br>
                            y envíelo a <a href="mailto:soporte@keepinagency.com">soporte@keepinagency.com</a>
                            <br><br>
                            De necesitar solución urgente puede comunicarse al<br>
                            <a href="tel:+584128001379">
                                <i class="fas fa-phone-square-alt pr-1"></i>04128001379
                            </a>
                            <a href="https://wa.me/+584128001379" class="pl-4" target="_blank">
                                <i class="fab fa-whatsapp-square pr-1"></i>04128001379
                            </a>
                            <br><br>
                          </span>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
  </div>
  
</body>
<!--body>
    <div id="container">
        <div id="header">
            <h1><?= __('Error') ?></h1>
        </div>
        <div id="content">
            <?php //$this->Flash->render() ?>

            <?php //$this->fetch('content') ?>
        </div>
        <div id="footer">
            <?php //$this->Html->link(__('Back'), 'javascript:history.back()') ?>
        </div>
    </div>
</body-->
</html>
