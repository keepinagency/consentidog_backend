<?php
use Cake\Core\Configure;
?>
<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <?= $this->Html->meta('icon') ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Consentidog - Para tu mejor amigo, el mejor lugar
    <?php $this->assign('title', $title); ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <!-- CSS Files -->
  <?= $this->Html->css('bootstrap.min.css') ?>
  <?= $this->Html->css('paper-dashboard.css') ?>
  <?= $this->Html->css('paper-kit-lite.css') ?>
  <?= $this->Html->css('all.css') ?>
  <?= $this->Html->css('keepincustom.css') ?>

  <?= $this->Html->css('fullcalendar.css') ?>
  <?= $this->Html->css('fullcalendar.print.css') ?>
    
  <!--   Core JS Files   -->
  <?= $this->Html->script(['jquery.min.js','popper.min.js', 'bootstrap.min.js',
                           'perfect-scrollbar.jquery.min.js', 'paper-dashboard.min.js', 
                           'chartjs.min.js', 'moment.min.js', 'bootstrap-datepicker.js']) ?>

  <?php
  $urlact=$this->request->here; 
  if ( strpos($urlact,'dashboard')>0) {
    echo $this->Html->script(['jquery-1.10.2.js','jquery-ui.custom.min.js','fullcalendar.js']);
  }
  ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>

  <script src="https://kit.fontawesome.com/2f4013e4ee.js" crossorigin="anonymous"></script>


  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a class="simple-text logo-mini" href="" >
          <div class="logo-image-small">
            <?php echo $this->Html->image('logo-small.png', ['alt' => 'Consentidog'], ['controller' => 'Users' ,'action' => 'dashboard']); ?>
          </div>
        </a>
        <div class="simple-text logo-normal">
            <?php echo $this->Html->link(__('Consentidog'), ['controller' => 'Users' ,'action' => 'dashboard'])?>
        </div>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li <?php echo ($this->request->getParam('controller') == 'Users' && $this->request->getParam('action') == 'dashboard') ? "class='active'" : "" ?>>
            <?= $this->Html->link('<i class="nc-icon nc-shop"></i><p>'.__('Home').'</p>', ['controller' => 'Users' ,'action' => 'dashboard'], ['escape' => false]) ?>
          </li>
          <li <?php echo ($this->request->getParam('controller') == 'PetsServices') ? "class='active'" : "" ?>>
            <?= $this->Html->link('<i class="nc-icon nc-calendar-60"></i><p>'.__('PetsServices').'</p>', ['controller' => 'PetsServices' ,'action' => 'index'], ['escape' => false]) ?>
          </li>
          <?php if ($Auth->user('role_id') == Configure::read('ROLES.ADMIN')): ?>
            <li <?php echo ($this->request->getParam('controller') == 'Movements') ? "class='active'" : "" ?>>
              <?= $this->Html->link('<i class="nc-icon nc-money-coins"></i><p>'.__('Movements').'</p>', ['controller' => 'Movements' ,'action' => 'index'], ['escape' => false]) ?>
            </li>
          <?php endif; ?>
          <li <?php echo ($this->request->getParam('controller') == 'Pets') ? "class='active'" : "" ?>>
            <?= $this->Html->link('<i class="nc-icon nc-tie-bow"></i><p>'.__('Pets').'</p>', ['controller' => 'Pets' ,'action' => 'index'], ['escape' => false]) ?>
          </li>
          <?php if ($Auth->user('role_id') == Configure::read('ROLES.ADMIN')): ?>
            <li <?php echo ($this->request->getParam('controller') == 'Users' && $this->request->getParam('action') != 'dashboard') ? "class='active'" : "" ?>>
              <?= $this->Html->link('<i class="nc-icon nc-single-02"></i><p>'.__('Users').'</p>', ['controller' => 'Users' ,'action' => 'index'], ['escape' => false]) ?>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#title"><?= $this->fetch('title') ?></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://consentidog.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-settings-gear-65"></i>
                  <p>
                    <span class="d-lg-none d-md-block"><?= __('Settings') ?></span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <?php if ($Auth->user('role_id') == Configure::read('ROLES.ADMIN')): ?>
                  <?= $this->Html->link(__('Services'), ['controller' => 'Services' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Countries'), ['controller' => 'Countries' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('States'), ['controller' => 'States' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Cities'), ['controller' => 'Cities' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Currencies'), ['controller' => 'Currencies' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Breeds'), ['controller' => 'Breeds' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Colours'), ['controller' => 'Colours' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?= $this->Html->link(__('Empleados'), ['controller' => 'Employees' ,'action' => 'index'], ['class'=>'dropdown-item']) ?>
                  <?php endif ?>
                </div>
              </li>
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-single-02"></i>
                  <p>
                    <span class="d-lg-none d-md-block"><?= __('My Account') ?></span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <?= $this->Html->link(__('My Profile'), ['controller' => 'Users', 'action' => 'view', $Auth->user('id')], ['class' => 'dropdown-item']) ?>
                  <?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item']) ?>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content pl-3 pr-3">
        <div class="row col-md-12">
            <?= $this->Flash->render() ?>
        </div>
        <div class="row ">
            <?= $this->fetch('content') ?>
        </div>
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, Powered by Keepin
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <?= $this->Html->script(['bootstrap-filestyle.min.js']) ?>
</body>

</html>
