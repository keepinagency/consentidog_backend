
<!--
=========================================================
 Paper Kit 2 - v2.2.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-kit-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-kit-2/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <?= $this->Html->meta('icon') ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Consentidog - Para tu mejor amigo, el mejor lugar
    <?php $this->assign('title', $title); ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <?= $this->Html->css('bootstrap.min.css') ?>
  <?= $this->Html->css('paper-kit.min.css') ?>
  <?= $this->Html->css('paper-kit-lite.css') ?>
    
  <!--   Core JS Files   -->
  <?= $this->Html->script(['jquery.min.js','popper.min.js', 'bootstrap.min.js','perfect-scrollbar.jquery.min.js']) ?>

  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>
</head>

<body class=" sidebar-collapse"> <!-- register-page -->
  <!-- Navbar -->
  <!--nav class="navbar navbar-expand-lg fixed-top navbar-transparent " color-on-scroll="300">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="#" rel="tooltip" title="Consentidog" data-placement="bottom" target="">
          Consentidog
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/vipconsentidogccs" target="_blank">
              <i class="fa fa-instagram"></i>
              <p class="d-lg-none">Instagram</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav-->
  <div class="page-header" style="background-image: url('../img/slide-home-09-1200x600.jpg');">
    <div class="filter"></div> 
    <div class="container">
        <div class="row">
            <div class="col-lg-4 ml-auto p-0">
                <div class="card card-register">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
                <div class="footer register-footer text-center">
                  <h6>©
                    <script>
                      document.write(new Date().getFullYear())
                    </script>, Desarollado por <a href="https://keepinagency.com/" target="_blank">Keepin Agency </a>.
                </div>
            </div>
        </div>
    </div>
    
  </div>
</body>

</html>
