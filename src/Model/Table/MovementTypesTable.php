<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MovementTypes Model
 *
 * @property \App\Model\Table\MovementsTable|\Cake\ORM\Association\HasMany $Movements
 *
 * @method \App\Model\Entity\MovementType get($primaryKey, $options = [])
 * @method \App\Model\Entity\MovementType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MovementType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MovementType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MovementType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MovementType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MovementType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MovementType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MovementTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('movement_types');
        $this->setDisplayField('display_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Movements', [
            'foreignKey' => 'movement_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('display_name')
            ->maxLength('display_name', 45)
            ->requirePresence('display_name', 'create')
            ->notEmptyString('display_name');

        return $validator;
    }
}
