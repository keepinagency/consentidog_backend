<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
//use Cake\Search\Manager;

/**
 * PetsServices Model
 *
 * @property \App\Model\Table\PetsTable|\Cake\ORM\Association\BelongsTo $Pets
 * @property \App\Model\Table\ServicesTable|\Cake\ORM\Association\BelongsTo $Services
 *
 * @method \App\Model\Entity\PetsService get($primaryKey, $options = [])
 * @method \App\Model\Entity\PetsService newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PetsService[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PetsService|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PetsService saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PetsService patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PetsService[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PetsService findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PetsServicesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pets_services');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        // Add the behaviour to your table

        $this->belongsTo('Pets', [
            'foreignKey' => 'pet_id',
            'joinType' => 'LEFT'
        ]);
        
        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType' => 'LEFT',
        ]);

        /*$this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Movements', [
            'foreignKey' => 'movement_id',
            'joinType' => 'INNER',
        ]);*/

        $this->addBehavior('Search.Search');
        // Setup search filter using search manager

        $this->searchManager()
            ->value('pet_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['Pets.name','Services.name',
                            'PetsServices.start_date',
                            'PetsServices.end_date']
            ])
            /*->add('consulta', 'Search.Compare', [
                'field' => ['PetsServices.name'],
                'comparison' => '>='
                
            ])*/;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('start_date')
            ->allowEmptyDateTime('start_date');

        $validator
            ->dateTime('end_date')
            ->allowEmptyDateTime('end_date');

        $validator
            ->boolean('confirmed')
            ->requirePresence('confirmed', 'create')
            ->notEmptyString('confirmed');

        $validator
            ->boolean('executed')
            ->allowEmptyString('executed');

        /*$hoy = date('d/m/Y h:i A');
        $validator->add('masked_start_date', 'custom', [
            'rule' => function ($value, $context) use ($hoy){
                $value=strtotime($value);
                $hoy=strtotime($hoy);            
                if ($value <= $hoy) {
                    return 'La fecha de inicio no puede ser anterior a la fecha actual';
                    //return 'La fecha de inicio ' .$value. 'no puede ser anterior a la fecha actual'.$hoy;
                }
                return true;
            },
            'message' => 'Generic error message used when `false` is returned'
        ]);*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pet_id'], 'Pets'));
        $rules->add($rules->existsIn(['service_id'], 'Services'));
        /*$rules->add($rules->existsIn(['employee_id'], 'Employees'));
        /*$rules->add($rules->existsIn(['movements_id'], 'Movements'));*/

        return $rules;
    }
}
