<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pets Model
 *
 * @property \App\Model\Table\GendersTable|\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ColoursTable|\Cake\ORM\Association\BelongsTo $Colours
 * @property \App\Model\Table\BreedsTable|\Cake\ORM\Association\BelongsToMany $Breeds
 * @property \App\Model\Table\ServicesTable|\Cake\ORM\Association\BelongsToMany $Services
 * @property \App\Model\Table\PetsServicesTable|\Cake\ORM\Association\HasMany $PetsService
 *
 * @method \App\Model\Entity\Pet get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pet newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pet[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pet|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pet saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pet patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pet[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pet findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PetsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pets');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Colours', [
            'foreignKey' => 'colour_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Breeds', [
            'foreignKey' => 'pet_id',
            'targetForeignKey' => 'breed_id',
            'joinTable' => 'breeds_pets'
        ]);
        $this->belongsToMany('Services', [
            'foreignKey' => 'pet_id',
            'targetForeignKey' => 'service_id',
            'joinTable' => 'pets_services'
        ]);
        $this->hasMany('PetsServices', [
            'foreignKey' => 'pet_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        
        // Add the behaviour and configure any options you want
        /*
        $this->addBehavior('Proffer.Proffer', [
            'photo' => [	// The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir',	// The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [	// Define the prefix of your thumbnail
                        'w' => 300,	// Width
                        'h' => 300,	// Height
                        'crop' => true,
                        'orientate' => true,
                        'jpeg_quality'	=> 100
                    ]
                ],
                'thumbnailMethod' => 'gd'	// Options are Imagick or Gd
            ]
        ]);
        */

        /*
        $this->belongsTo('Pets',[
            'foreignKey' => 'pets_id', 
            'joinType' => 'INNER'
            ]);*/

        $this->addBehavior('Search.Search');
        // Setup search filter using search manager

        $this->searchManager()
            ->value('pet_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['Pets.name']
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator):Validator
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', null, 'true');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'true')
            ->notEmptyString('name');

        $validator
            ->boolean('sterile')
            ->requirePresence('sterile', 'true');
            //->notEmptyString('sterile');
        
        $validator
            ->integer('gender_id')
            ->requirePresence('gender_id', 'true')
            ->notEmpty('gender_id');

        $validator
            ->dateTime('birth_date')
            //->requirePresence('birth_date', 'true')
            ->notEmptyDateTime('birth_date');

        $validator
            ->numeric('weight')
            ->requirePresence('weight', 'true')
            ->notEmpty('weight');

        $validator
            ->scalar('additional_information')
            ->allowEmptyString('additional_information');

        $validator
            // Set the thumbnail resize dimensions
            ->allowEmptyFile('photo', 'create')
            ->add('photo', 'file', [
                'rule' => ['fileSize', '<=','1MB'],
                /*
                'rule' => ['uploadedFile', ['optional' => true]],
                'rule' => ['dimensions', [
                    'min' => ['w' => 100, 'h' => 100],
                    'max' => ['w' => 1200, 'h' => 1200]
                ]],
                'provider' => 'proffer'
                */
                'message' => 'Por favor seleccione una imagen menor a 2mb',
                
            ]);

            /*Validación para Weigth*/
        $validator->add('weight', 'custom', [
            'rule' => function ($value, $context) {            
                if ($value < 4) {
                    return 'El peso minimo debe ser mayor a 3 kilos';
                }
                if ($value > 90) {
                    return 'El peso maximo debe ser menor a 90 kilos';
                }
                return true;
            },
            'message' => 'Generic error message used when `false` is returned'
        ]);
            /*Validación para Birth_date*/
        $hoy = strtotime(date('m/d/Y h:i A'));
        $ano = mktime(0, 0, 0, date("m"), date("d"), date("Y")-20);
        
        $validator->add('masked_birth_date', 'custom', [
            'rule' => function ($value, $context) use ($hoy, $ano){   
                //echo strtotime($value). "----" .$ano; 
                //die();        
                if (strtotime($value) > $hoy) {
                    return 'La fecha es incorrecta, por favor verifique';
                }else {
                    if (strtotime($value) < $ano) {
                        return '¿Seguro que el perro tiene mas de 20 años?, por favor verifique';
                    }
                }
                return true;
            },
            'message' => 'Generic error message used when `false` is returned'
        ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['gender_id'], 'Genders'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['colour_id'], 'Colours'));

        return $rules;
    }
}
