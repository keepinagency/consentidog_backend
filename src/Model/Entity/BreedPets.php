<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Breed Entity
 *
 * @property int $id
 * @property int $breed_id
 * @property int $pet_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Pet[] $pets
 * @property \App\Model\Entity\Breeds[] $breeds
 */
class Breed extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'breed_id' => true,
        'pet_id' => true,
        'created' => true,
        'modified' => true,
        'pets' => true,
        'breeds' => true
    ];
}
