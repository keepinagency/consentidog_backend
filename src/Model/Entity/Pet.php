<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pet Entity
 *
 * @property int $id
 * @property string $name
 * @property int $gender_id
 * @property bool $sterile
 * @property \Cake\I18n\FrozenTime $birth_date
 * @property float $weight
 * @property string|null $additional_information
 * @property int $user_id
 * @property int $colour_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Colour $colour
 * @property \App\Model\Entity\Breed[] $breeds
 * @property \App\Model\Entity\Service[] $services
 */
class Pet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'gender_id' => true,
        'sterile' => true,
        'birth_date' => true,
        'weight' => true,
        'additional_information' => true,
        'user_id' => true,
        'colour_id' => true,
        'created' => true,
        'modified' => true,
        'gender' => true,
        'user' => true,
        'colour' => true,
        'breeds' => true,
        'services' => true,
        'photo' => true,
        'photo_dir' => true
    ];
}
