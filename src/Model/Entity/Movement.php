<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movement Entity
 *
 * @property int $id
 * @property string $concept
 * @property float $amount
 * @property int $movement_type_id
 * @property int $currency_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $pets_service_id
 *
 * @property \App\Model\Entity\MovementType $movement_type
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\PetsService $pets_service
 */
class Movement extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'concept' => true,
        'amount' => true,
        'movement_type_id' => true,
        'currency_id' => true,
        'created' => true,
        'modified' => true,
        'pets_service_id' => true,
        'movement_type' => true,
        'currency' => true,
        'pets_service' => true
    ];
}
