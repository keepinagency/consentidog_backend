<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $address_line_1
 * @property string|null $address_line_2
 * @property int $city_id
 * @property string $postal_code
 * @property string $phone
 * @property string $mobile
 * @property int $role_id
 * @property bool $active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string|null $modified
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\EmergencyContact[] $emergency_contacts
 * @property \App\Model\Entity\Pet[] $pets
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'username' => true,
        'password' => true,
        'name' => true,
        'address_line_1' => true,
        'address_line_2' => true,
        'city_id' => true,
        'postal_code' => true,
        'phone' => true,
        'mobile' => true,
        'role_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'city' => true,
        'role' => true,
        'emergency_contacts' => true,
        'pets' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    // Hash Password
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }
}
