<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PetsService Entity
 *
 * @property int $id
 * @property int $pet_id
 * @property int $service_id
 * @property int $employee_id
 * @property \Cake\I18n\FrozenTime|null $start_date
 * @property \Cake\I18n\FrozenTime|null $end_date
 * @property bool $confirmed
 * @property bool|null $executed
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Pet[] $pet
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\Employee $employee
 */
class PetsService extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'pet_id' => true,
        'service_id' => true,
        'employee_id' => true,
        'start_date' => true,
        'end_date' => true,
        'confirmed' => true,
        'executed' => true,
        'created' => true,
        'modified' => true,
        'pet' => true,
        'service' => true,
        
    ];
}
