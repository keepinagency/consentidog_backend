<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Colours Controller
 *
 * @property \App\Model\Table\ColoursTable $Colours
 *
 * @method \App\Model\Entity\Colour[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ColoursController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }
    public function isAuthorized($user) 
    {
        return true;    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', __('Color List'));
        $this->viewBuilder()->setLayout('admin');

        $colours = $this->paginate($this->Colours);

        $this->set(compact('colours'));
    }

    /**
     * View method
     *
     * @param string|null $id Colour id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $this->set('title', 'Listado de Colores');
        $this->viewBuilder()->setLayout('admin');

        $colour = $this->Colours->get($id, [
            'contain' => ['Pets']
        ]);

        $this->set('colour', $colour);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Agregar Color'));
        $this->viewBuilder()->setLayout('admin');

        $colour = $this->Colours->newEntity();
        if ($this->request->is('post')) {
            $colour = $this->Colours->patchEntity($colour, $this->request->getData());
            if ($this->Colours->save($colour)) {
                $this->Flash->success(__('The colour has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The colour could not be saved. Please, try again.'));
        }
        $this->set(compact('colour'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Colour id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Modificar Color');
        $this->viewBuilder()->setLayout('admin');

        $colour = $this->Colours->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $colour = $this->Colours->patchEntity($colour, $this->request->getData());
            if ($this->Colours->save($colour)) {
                $this->Flash->success(__('The colour has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The colour could not be saved. Please, try again.'));
        }
        $this->set(compact('colour'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Colour id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete Color'));
        $this->viewBuilder()->setLayout('admin');

        $this->request->allowMethod(['post', 'delete']);
        $colour = $this->Colours->get($id);
        if ($this->Colours->delete($colour)) {
            $this->Flash->success(__('The colour has been deleted.'));
        } else {
            $this->Flash->error(__('The colour could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
