<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Date;

/**
 * Pets Controller
 *
 * @property \App\Model\Table\PetsTable $Pets
 *
 * @method \App\Model\Entity\Pet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PetsController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Crud->addListener('Crud.Search', [
            // Events to listen for and apply search finder to query.
            'enabled' => [
                'Crud.beforeLookup',
                'Crud.beforePaginate'
            ],
            // Search collection to use
            'collection' => 'default'
        ]);
  
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'index','mypets'
            ],
            'listeners' => [
                'Crud.Search'
            ]
        ]);
    }

    public function isAuthorized($user) 
    {
        //print_r($user); 
        if ($user['role_id'] == Configure::read('ROLES.ADMIN') || $user['role_id'] == Configure::read('ROLES.USER'))
            return true;
        /*if ($user['role_id'] == Configure::read('ROLES.USER') && $this->request->getParam('action') === ('index'))
            return true;*/
        // Default 
        return parent::isAuthorized($user);
    }
    

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        
        $this->set('title', __('Pet List'));
        $this->viewBuilder()->setLayout('admin');
        $pets = $this->Pets->find();
        //$pets  = $query->contain(['Genders', 'Users', 'Colours','Services','PetsServices']);
        
        $uid = $this->request->getQuery('uid');
        if ($uid){
            $pets  = $pets->where(['Pets.user_id' => $uid]);
        }

        $q = $this->request->getQuery('q');
        if ($q){
            $qc = '%'.$q.'%';
            $pets = $pets->where(['Pets.name LIKE ' => $qc]);    
        }
        //$pets = $pets->find();

        $this->paginate = [
                    'contain' => ['Users','Colours'],
                    'limit' => '5',
                    'sort' => 'id',
                    'direction' => 'DESC'
                ];
        
                
        $pets = $this->paginate($pets);
        $this->set(compact('pets'));
        
        /*
        $query = $this->Pets
            // Use the plugins 'search' custom finder and pass in the
            // processed query params
            ->find('search', ['search' => $this->request->getQueryParams()])
            // You can add extra things to the query if you need to
            //->contain(['Comments'])
            ->where(['user_id IS NOT' => null]);

            
            $this->set('pets', $this->paginate($query));
        //$this->set(compact('pets'));
        */
    }

    /**
     * View method
     *
     * @param string|null $id Pet id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $idusu = null)
    {
        $this->set('title', __('View Pet'));
        $this->viewBuilder()->setLayout('admin');

        $pet = $this->Pets->get($id, [  //'contain' => ['Pets', 'Services']
                                'contain' => ['Users', 'Genders', 'Colours', 'Breeds', 'PetsServices.Services'],
                            ]);

        $this->set('pet', $pet);
        //$this->set('pet', $this->paginate($pet));

        /*$pet = $this->Pets->get($id, [
            'contain' => ['Genders', 'Users', 'Colours', 'Breeds', 'PetsServices','Services']]);
        if (!empty($idusu)){
            $query = $this->Pets
                // You can add extra things to the query if you need to
                //->contain(['Comments'])
                ->where(['user_id IS ' => $idusu]);
        }else{
            $query = $this->Pets
                // You can add extra things to the query if you need to
                //->contain(['Comments'])
                ->where(['user_id IS NOT' => null]);
        }
        $this->set('Pets', $this->paginate($query));
        $this->set(compact('Pets'));
        $this->set('pet', $pet);*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add Pet'));
        $this->viewBuilder()->setLayout('admin');

        $pet = $this->Pets->newEntity();
        if ($this->request->is('post')) {
            $pet = $this->Pets->patchEntity($pet, $this->request->getData());
            $pet->birth_date = new Date($this->request->getData('masked_birth_date'));
            if ($this->Pets->save($pet)) {
                $this->Flash->success(__('The pet has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pet could not be saved. Please, try again.'));
        }
        $genders = $this->Pets->Genders->find('list', ['limit' => 200]);
        $users = $this->Pets->Users->find('list', ['limit' => 200]);
        $colours = $this->Pets->Colours->find('list', ['limit' => 200]);
        $breeds = $this->Pets->Breeds->find('list', ['limit' => 200]);
        $services = $this->Pets->Services->find('list', ['limit' => 200]);
        $this->set(compact('pet', 'genders', 'users', 'colours', 'breeds', 'services'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pet id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', __('Edit Pet'));
        $this->viewBuilder()->setLayout('admin');

        $pet = $this->Pets->get($id, [
            'contain' => ['Breeds', 'Services']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pet = $this->Pets->patchEntity($pet, $this->request->getData());
            $pet->birth_date = new Date($this->request->getData('masked_birth_date'));
            if ($this->Pets->save($pet)) {
                $this->Flash->success(__('The pet has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pet could not be saved. Please, try again.'));
        }
        $genders = $this->Pets->Genders->find('list', ['limit' => 200]);
        $users = $this->Pets->Users->find('list', ['limit' => 200]);
        $colours = $this->Pets->Colours->find('list', ['limit' => 200]);
        $breeds = $this->Pets->Breeds->find('list', ['limit' => 200]);
        $services = $this->Pets->Services->find('list', ['limit' => 200]);
        $this->set(compact('pet', 'genders', 'users', 'colours', 'breeds', 'services'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pet id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete Pet'));
        $this->viewBuilder()->setLayout('admin');

        $this->request->allowMethod(['post', 'delete']);
        $pet = $this->Pets->get($id);
        if ($this->Pets->delete($pet)) {
            $this->Flash->success(__('The pet has been deleted.'));
        } else {
            $this->Flash->error(__('The pet could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
