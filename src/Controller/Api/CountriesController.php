<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;

class CountriesController extends AppController
{
    public $paginate = [
        'page' => 1,
        'limit' => 5,
        'maxLimit' => 15,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];

    public function index()
    {
        /*$this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->contain([
                'states'
            ]);
        });*/
        return $this->Crud->execute();
    }
}