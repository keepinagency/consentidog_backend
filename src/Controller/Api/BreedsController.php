<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class BreedsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'token']);
    }

    /*public $paginate = [
        'page' => 1,
        'limit' => 5,
        'maxLimit' => 15,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];*/

    public function all()
    {
        $this->Crud->on('beforePaginate', function(Event $event) {
            /*$arr_args=$this->request->getParam('pass');
            $uid = $arr_args[0];
            $event->subject->query = $event->subject->query
                        ->contain(['Genders', 'Colours'])
                        ->where(['user_id' => $uid]);*/
        });
        return $this->Crud->execute('index');
    }

    public function index()
    {
        /*$this->Crud->on('beforeFind', function (Event $event) {
            debug("hola");
            /*$event->getSubject()->query->contain([
            'states'
            ]);
        });*/
        return $this->Crud->execute();
    }

    
}