<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;

class PetsServicesController extends AppController
{
    public $paginate = [
        'page' => 1,
        'limit' => 3,
        'sort' => 'start_date',
        'direction' => 'DESC'
    ];

    public function index()
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->contain([
            'Pets.Users' => function ($qq){return $qq->limit(4);}
            ]);
        });
        
        return $this->Crud->execute();
    }

    public function view()
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->contain([
            'Pets',
            ]);
        });
        return $this->Crud->execute();
    }

}
