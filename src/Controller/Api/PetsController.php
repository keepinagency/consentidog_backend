<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
/*
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;
use Firebase\JWT\JWT;
*/

class PetsController extends AppController
{
    /*public $paginate = [
        'page' => 1,
        'limit' => 5,
        'maxLimit' => 15,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];*/

    /*public function index()
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->contain([
                'states'
            ]);
        });
        return $this->Crud->execute();
    }*/



    public function index($arg1)
    {
        
        /*
        $this->Crud->on('beforeFind', function (Event $event) {
            //$arr_args=$this->request->getParam('pass');
            $uid = $arg1;
            //die($uid);
            //$event->subject->query = $event->subject->query
            // ->contain(['Genders', 'Colours'])
            $event->getSubject()->query->where(['user_id' => 25]);
        });
        
        $uid = $arg1;
        $pets = $this->Pets->find()
                        ->contain(['Genders', 'Colours'])
                        ->where(['user_id' => $uid]);*/
        $this->Crud->on('beforeFind', function(\Cake\Event\Event $event) {
            $event->subject->query->where(['user_id' => 25]);
        });
        //die('sdsd');
        return $this->Crud->execute();
    }

    public function services($arg1)
    {
        $this->Crud->on('beforePaginate', function(Event $event) {
            $arr_args=$this->request->getParam('pass');
            $uid = $arr_args[0];
            $event->subject->query = $event->subject->query
                        ->contain(['Genders', 'Colours','PetsServices.Services'])
                        ->where(['user_id' => $uid]);
                        //->orderDesc('start_date');
            
            //$event->subject->toList();
            
                        
            /*print_r($event);
            die();
            /* * */
                        //
        });
        return $this->Crud->execute();
        //die('------');
    }

    public function view($idusu = null)
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query
                    ->contain(['Services','Users']);
        });
        return $this->Crud->execute();
    }

    public function add()
    {
  
        $this->Crud->on('beforeSave', function(Event $event ) {
            
            
            $img = $this->request->data['photo64'];
            $data = base64_decode($img);

            //if (empty($fileType)){
                $fileType = "jpg";
            //}
            
            $filename = $this->request->data['name'].'photo.' . $fileType;

            $codfecha = date('Ymd');
            $folder_name =  $codfecha.'-'.$this->request->data['name'];

            $birth_date = $this->request->data['birth_date'];
            //debug($birth_date);
            //debug($event->subject->entity->birth_date);

            $fechamysql = date("Y/m/d h:i:s",strtotime(date($birth_date)));
            //debug($fechamysql);
            
            $event->subject->entity->birth_date = $fechamysql;
            $event->subject->entity->photo = $filename;
            $event->subject->entity->photo_dir = $folder_name;
            
            /*debug($this->request->data);
            debug($event->subject);
            die();*/

            //debug($event->subject);
            //return false;
        });

        $this->Crud->on('afterSave', function(Event $event) {

            if ($event->subject->success){
                $img = $this->request->data['photo64'];
                $data = base64_decode($img);

                //if (empty($fileType)){
                    $fileType = "jpg";
                //}

                $filename = $this->request->data['name'].'photo.' . $fileType;

                $codfecha = date('Ymd');
                $folder_name =  $codfecha.'-'.$this->request->data['name'];
                $file_dir = WWW_ROOT .'files/pets/photo/' . $folder_name.'/';            
                if (!is_dir($file_dir)){
                    mkdir($file_dir,0755);
                }

                file_put_contents($file_dir.$filename, $data);
                file_put_contents($file_dir.'square_'.$filename, $data);
                
                // Guardar razas asociadas
                $razas = $this->request->data['breed_id'];
                if (!is_array($razas)){
                    $razas = json_decode($razas,true);
                }
                if ( count($razas)>0 ){
                    $BreedsPetsTable = TableRegistry::get('BreedsPets');
                    foreach($razas as $breed_id){
                        $BreedsPets= $BreedsPetsTable->newEntity();
                        $BreedsPets->pet_id = $event->subject->entity->id;
                        $BreedsPets->breed_id = $breed_id;
                        if ($BreedsPetsTable->save($BreedsPets)) {
                            // The $article entity contains the id now
                            $id = $BreedsPets->id;
                            //debug($id);
                        }
                    }
                }
            }

            /*if ($event->subject->created) {
                
            }*/
        });

        return $this->Crud->execute();
    }

    public function edit($id)
    {
        /*echo $this->request->data['photo_dir'];
        die('');*/
        $this->Crud->on('beforeSave', function(Event $event ) {

            /*echo $event->subject->entity->photo_dir;
            die();*/
            
            
            $img = $this->request->data['photo64'];
            $data = base64_decode($img);

            //if (empty($fileType)){
                $fileType = "jpg";
            //}
            
            $filename = rawurlencode($this->request->data['name']).'photo.' . $fileType;
            //die();

            $codfecha = date('Ymd');
            // $folder_name = $this->request->data['photo_dir'];
            $folder_name = $event->subject->entity->photo_dir;
            
            if (!$folder_name){
                $folder_name =  $codfecha.'-'.rawurlencode($this->request->data['name']);
            }

            $event->subject->entity->photo = $filename;
            $event->subject->entity->photo_dir = $folder_name;

        });

        $this->Crud->on('afterSave', function(Event $event) {

            if ($event->subject->success){
                $img = $this->request->data['photo64'];
                $data = base64_decode($img);

                //if (empty($fileType)){
                    $fileType = "jpg";
                //}

                $filename = rawurlencode($this->request->data['name']).'photo.' . $fileType;

                $codfecha = date('Ymd');
                //$folder_name = $this->request->data['photo_dir'];
                $folder_name = $event->subject->entity->photo_dir;

                if (empty($folder_name)){
                    $folder_name =  $codfecha.'-'.$this->request->data['name'];
                }

                $file_dir = WWW_ROOT .'files/pets/photo/' . $folder_name.'/';            
                if (is_dir($file_dir)){
                    if (is_dir($file_dir)) {
                        $objects = scandir($file_dir);
                        foreach ($objects as $object) {
                          if ($object != "." && $object != "..") {
                            if (filetype($file_dir."/".$object) == "dir") rrmdir($file_dir."/".$object); else unlink($file_dir."/".$object);
                          }
                        }
                        reset($objects);
                        rmdir($file_dir);
                    }
                    mkdir($file_dir,0755);
                }else{
                    mkdir($file_dir,0755);
                }

                file_put_contents($file_dir.$filename, $data);
                file_put_contents($file_dir.'square_'.$filename, $data);
                

            }

        });

        return $this->Crud->execute('edit');
    }

    /*public function implementedEvents() {
        return [
            'fileUploaded' => 'handleUpload'
        ];
    }
    
    public function handleUpload(Event $event) {
        $file = $event->getData();
        $addFields = [
            'url' => 'some'
        ];
        $event->subject->eventManager()->dispatch(new Event('fileProcessed', $this, $addFields));
    }*/

}
