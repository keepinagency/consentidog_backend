<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'token']);
    }
    
    /*public $paginate = [
        'page' => 1,
        'limit' => 3,
        'maxLimit' => 5,
        'sort' =>'PetsServices__id',
        'direction' => 'desc'
    ];*/
    
    public function view()
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            /*
            $event->getSubject()->query
                ->contain(['Pets.PetsServices.Services','Pets.Breeds', 'Pets.Genders']);
             */  
                /*->order([
                    'PetsServices.id' => 'DESC']);*/
                /*->where(['pet_id IS NOT' => null]);*/
                //echo $this->Paginator->sort('PetsServices__service_id', ['direction'=>'asc']);

            
                //->find(['Pets.PetsServices.Services'=>['order' => 'start_date'],'Pets.Breeds', 'Pets.Genders']);
                //->orderDesc('PetsServices__start_date');
                //->orderDesc('PetsServices__service_id', ['direction'=>'asc']');
            });
            return $this->Crud->execute();
        

    }
    public function add()
    {
        $this->Crud->on('afterSave', function(Event $event) {
            if ($event->subject->created) {
                $this->set('data', [
                    'id' => $event->subject->entity->id,
                    'token' => JWT::encode(
                        [
                            'sub' => $event->subject->entity->id,
                            'exp' =>  time() + 604800
                        ],
                    Security::salt())
                ]);
                $this->Crud->action()->config('serialize.data', 'data');
            }
        });
        return $this->Crud->execute();
    }

    public function token()
    {
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException('Invalid username or password');
        }

        $this->set([
            'success' => true,
            'data' => [
                'token' => JWT::encode([
                    'sub' => $user['id'],
                    'exp' =>  time() + 604800
                ],
                Security::salt()),
                'uid' => $user['id'],
                'name' => $user['name'],
            ],
            '_serialize' => ['success', 'data']
        ]);
    }
}