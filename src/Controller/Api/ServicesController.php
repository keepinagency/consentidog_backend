<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class ServicesController extends AppController
{
    public $paginate = [
        //'page' => 1,
        //'limit' => 30,
        //'maxLimit' => 5,
        'sort' =>'name'
    ];
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'token']);
    }

    public function index()
    {
        /*$this->Crud->on('beforeFind', function (Event $event) {
            debug("hola");
            /*$event->getSubject()->query->contain([
            'states'
            ]);
        });*/
        return $this->Crud->execute();
    }
}