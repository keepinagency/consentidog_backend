<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MovementTypes Controller
 *
 * @property \App\Model\Table\MovementTypesTable $MovementTypes
 *
 * @method \App\Model\Entity\MovementType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MovementTypesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }
    public function isAuthorized($user) 
    {
        return true;    
    }   
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', __('Movement Type List'));
        $this->viewBuilder()->setLayout('admin');

        $movementTypes = $this->paginate($this->MovementTypes);

        $this->set(compact('movementTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Movement Type id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', __('View movement type'));
        $this->viewBuilder()->setLayout('admin');

        $movementType = $this->MovementTypes->get($id, [
            'contain' => ['Movements']
        ]);

        $this->set('movementType', $movementType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add movement type'));
        $this->viewBuilder()->setLayout('admin');

        $movementType = $this->MovementTypes->newEntity();
        if ($this->request->is('post')) {
            $movementType = $this->MovementTypes->patchEntity($movementType, $this->request->getData());
            if ($this->MovementTypes->save($movementType)) {
                $this->Flash->success(__('The movement type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movement type could not be saved. Please, try again.'));
        }
        $this->set(compact('movementType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Movement Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', __('Edit movement type'));
        $this->viewBuilder()->setLayout('admin');

        $movementType = $this->MovementTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movementType = $this->MovementTypes->patchEntity($movementType, $this->request->getData());
            if ($this->MovementTypes->save($movementType)) {
                $this->Flash->success(__('The movement type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movement type could not be saved. Please, try again.'));
        }
        $this->set(compact('movementType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Movement Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete movement type'));
        $this->viewBuilder()->setLayout('admin');
        
        $this->request->allowMethod(['post', 'delete']);
        $movementType = $this->MovementTypes->get($id);
        if ($this->MovementTypes->delete($movementType)) {
            $this->Flash->success(__('The movement type has been deleted.'));
        } else {
            $this->Flash->error(__('The movement type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
