<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /*public function beforeFilter(\Cake\Event\Event $event) {
        $this->Crud->addListener('Crud.Search', [
            // Events to listen for and apply search finder to query.
            'enabled' => [
                'Crud.beforeLookup',
                'Crud.beforePaginate'
            ],
            // Search collection to use
            'collection' => 'default'
        ]);
  
        parent::beforeFilter($event);
    }*/

    public function beforeFilter(Event $event)
    {
        $this->Crud->addListener('Crud.Search', [
            // Events to listen for and apply search finder to query.
            'enabled' => [
                'Crud.beforeLookup',
                'Crud.beforePaginate'
            ],
            // Search collection to use
            'collection' => 'default'
        ]);

        parent::beforeFilter($event);
        // Allow users to register and logout.
        $this->Auth->allow(['logout', 'add', 'getStates', 'getCities']);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'index'
            ],
            'listeners' => [
                'Crud.Search'
            ]
        ]);
    }

    public function isAuthorized($user) 
    {
        //print_r($user); 
        if ($user['role_id'] == Configure::read('ROLES.ADMIN') /*|| $user['role_id'] == Configure::read('ROLES.USER')*/){
            return true;
        }

        if ($user['role_id'] == Configure::read('ROLES.USER') && 
            $this->request->getParam('action') === 'dashboard'){
                return true;
        }
            
        if ($user['role_id'] == Configure::read('ROLES.USER') && 
            in_array($this->request->getParam('action'), ['view', 'edit']) &&
            $user['id'] == (int)$this->request->getParam('pass.0')){
                return true;
            }
        
        // Default 
        return parent::isAuthorized($user);
    }

    public function dayservices( $dia = '' ){
        $this->viewBuilder()->setLayout('ajax');
        //echo $dia;
        //die();
        if ($dia === ''){
            return false;
        }else{
            
            $connection = ConnectionManager::get('default');
            $results = $connection
                    ->execute('SELECT 
                                PetsServices.id, 
                                PetsServices.pet_id, 
                                PetsServices.service_id, 
                                PetsServices.start_date, 
                                PetsServices.end_date, 
                                PetsServices.confirmed, 
                                PetsServices.executed, 
                                Pets.id AS PetId, 
                                Pets.name AS PetName, 
                                Services.id AS ServiceId, 
                                Services.name AS ServiceName
                             FROM 
                                pets_services PetsServices 
                                    LEFT JOIN pets Pets 
                                        ON Pets.id = (PetsServices.pet_id) 
                                    LEFT JOIN services Services 
                                        ON Services.id = (PetsServices.service_id) 
                             WHERE
                                "'.$dia.'" >= DATE_SUB(PetsServices.start_date,INTERVAL 1 DAY)  AND 
                                    "'.$dia.'" <= PetsServices.end_date')
                    ->fetchAll('assoc');

            $this->set('petsServices', $results);
            $this->set('total_pets_service_confirmed', count($results));
            $this->set('dia', $dia);
            
            
            /*
            confirmed = 1 AND 
                                    executed = 0 AND 

            $this->loadModel('PetsServices');
            $query = $this->PetsServices->find()
                        ->where(['confirmed =' => true, 'executed =' => false,
                                '"'.$dia.'" >=' => h('DATE(PetsServices.start_date)'),
                                '"'.$dia.'" <=' => h('DATE(PetsServices.end_date)')
                                ]) 
                        ->contain(['Pets', 'Services']);
            $this->set('total_pets_service_confirmed', $query->count());
            $this->set('petsServices', $query);
            die();*/
        }

    }

    public function dashboard()
    {
        $this->set('title', __('Home'));
        $this->viewBuilder()->setLayout('admin');

        $this->loadModel('Users');
        $query = $this->Users->find();
        $this->set('total_users', $query->count());

        $this->loadModel('Pets');
        $query = $this->Pets->find();
        $this->set('total_pets', $query->count());

        
        $dia = date('Y-m-d');
        $connection = ConnectionManager::get('default');
        $results = $connection
                ->execute('SELECT 
                            PetsServices.id, 
                            PetsServices.pet_id, 
                            PetsServices.service_id, 
                            PetsServices.start_date, 
                            PetsServices.end_date, 
                            PetsServices.confirmed, 
                            PetsServices.executed, 
                            Pets.id AS PetId, 
                            Pets.name AS PetName, 
                            Services.id AS ServiceId, 
                            Services.name AS ServiceName
                            FROM 
                            pets_services PetsServices 
                                LEFT JOIN pets Pets 
                                    ON Pets.id = (PetsServices.pet_id) 
                                LEFT JOIN services Services 
                                    ON Services.id = (PetsServices.service_id) 
                            WHERE
                            "'.$dia.'" >= DATE_SUB(PetsServices.start_date,INTERVAL 1 DAY)  AND 
                                "'.$dia.'" <= PetsServices.end_date')
                ->fetchAll('assoc');

        $this->set('petsServicesDay', $results);
        $this->set('total_pets_service_confirmed', count($results));
        
        $this->loadModel('PetsServices');
        /*$query = $this->PetsServices->find()
                    //->where(['confirmed =' => true, 'executed =' => false])
                    ->where(['start_date >=' => $fechaact, 'end_date <=' => $fechaact ])
                    ->contain(['Pets', 'Services'])
                    ->limit(10);*/

        $query = $this->PetsServices->find()
                    ->contain(['Pets', 'Services'])
                    ->order(['service_id' => 'DESC']);
        $this->set('petsServicesFull', $query);


        $this->loadModel('Movements');
        $query = $this->Movements->find()->where(['movement_type_id =' => 1]);
        /*//$res =$query->select(['sum' => $query->func()->sum('Movements.amount')])->first();
        //$this->set('total_amount_income', $res->sum);*/
        $this->set('total_amount_income', $query->count());

        $query = $this->Movements->find()->where(['movement_type_id =' => 2]);
        //$res =$query->select(['sum' => $query->func()->sum('Movements.amount')])->first();
        //$this->set('total_amount_expenses', $res->sum);
        $this->set('total_amount_expenses', $query->count());

        $this->loadModel('Users');
        $query = $this->Users->find()->where(['active =' => true]);
        $this->set('total_users_active', $query->count());
        
        $linkdayservices = Router::url('/users/dayservices');
        $this->set('linkdayservices', $linkdayservices);
        
        //Servicios Ejecutados
        /*$this->loadModel('PetsServices');
        $query = $this->PetsServices->find()->where(['executed =' => false]);
        $this->set('total_pets_service_pending', $query->count());*/

    }
    
    public function login()
    {
        $this->set('title', 'Bienvenido');
        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Your username or password is incorrect.'));
        }
    }

    public function logout()
    {
        $this->Flash->success(__('You are now logged out.'));
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', __('User List'));
        $this->viewBuilder()->setLayout('admin');

        $this->paginate = [
            'contain' => ['Cities', 'Roles'],
            'limit' => '5',
            'sort' => 'id',
            'direction' => 'DESC'
        ];
        $users = $this->paginate($this->Users);
        $this->set(compact('users'));

        $query = $this->Users
            // Use the plugins 'search' custom finder and pass in the
            // processed query params
            ->find('search', ['search' => $this->request->getQueryParams()])
            // You can add extra things to the query if you need to
            //->contain(['Comments'])
            ->where(['role_id IS NOT' => null]);

        $this->set('users', $this->paginate($query));
        //$this->set(compact('pets'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', __('View User'));
        $this->viewBuilder()->setLayout('admin');

        $user = $this->Users->get($id, [
            'contain' => ['Cities', 'Roles', 'EmergencyContacts', 'Pets']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add User'));
        $this->viewBuilder()->setLayout('admin');

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            /*}else{
                var_dump($this->invalidFields());
                return false;*/
            }
            $this->Flash->error(__('Verifique los datos, el usuario no se ha podido guardar.'));
        }
        $countries = TableRegistry::get('Countries')->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $states = TableRegistry::get('States')->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $cities = TableRegistry::get('Cities')->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $this->set(compact('user', 'countries', 'roles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', __('Edit User'));
        $this->viewBuilder()->setLayout('admin');

        $user = $this->Users->get($id, [
            'contain' => [Cities => States]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Verifique los datos, el usuario no se ha podido guardar.'));
        }
        $countries = TableRegistry::get('Countries')->find('list');
        $states = TableRegistry::get('States')->find('list', ['limit' => 200]);
        $cities = $this->Users->Cities->find('list', [ 'conditions' => [ 'state_id' => $user->city->state_id ]]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'countries', 'states', 'cities', 'roles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getStates() 
    {
        print_r($this->request->data);
        $id = $this->request->data('id');
        $states = TableRegistry::get('States')->find('all', [ 'conditions' => [ 'country_id' => $id ] ]);
        echo json_encode($states);
        die();
    }
    
    public function getCities() 
    {
        $id = $this->request->data('id');
        $cities = TableRegistry::get('Cities')->find('all', [ 'conditions' => [ 'state_id' => $id ] ]);
        echo json_encode($cities);
    }
}
