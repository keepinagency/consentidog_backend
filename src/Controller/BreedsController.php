<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Date;

/**
 * Breeds Controller
 *
 * @property \App\Model\Table\BreedsTable $Breeds
 *
 * @method \App\Model\Entity\Breed[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BreedsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add']);
    }
    
    public function isAuthorized($user) 
    {
        //print_r($user); 
        if ($user['role_id'] == Configure::read('ROLES.ADMIN') || $user['role_id'] == Configure::read('ROLES.USER'))
            return true;
        /*if ($user['role_id'] == Configure::read('ROLES.USER') && $this->request->getParam('action') === ('index'))
            return true;*/
        // Default 
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', __('Breed List'));
        $this->viewBuilder()->setLayout('admin');

        $breeds = $this->paginate($this->Breeds);

        $this->set(compact('breeds'));
    }

    /**
     * View method
     *
     * @param string|null $id Breed id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Listado de Razas');
        $this->viewBuilder()->setLayout('admin');

        $breed = $this->Breeds->get($id, [
            'contain' => ['Pets']
        ]);

        $this->set('breed', $breed);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add Breed'));
        $this->viewBuilder()->setLayout('admin');

        $breed = $this->Breeds->newEntity();
        if ($this->request->is('post')) {
            $breed = $this->Breeds->patchEntity($breed, $this->request->getData());
            if ($this->Breeds->save($breed)) {
                $this->Flash->success(__('The breed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The breed could not be saved. Please, try again.'));
        }
        $pets = $this->Breeds->Pets->find('list', ['limit' => 200]);
        $this->set(compact('breed', 'pets'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Breed id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', __('Edit Breed'));
        $this->viewBuilder()->setLayout('admin');

        $breed = $this->Breeds->get($id, [
            'contain' => ['Pets']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $breed = $this->Breeds->patchEntity($breed, $this->request->getData());
            if ($this->Breeds->save($breed)) {
                $this->Flash->success(__('The breed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The breed could not be saved. Please, try again.'));
        }
        $pets = $this->Breeds->Pets->find('list', ['limit' => 200]);
        $this->set(compact('breed', 'pets'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Breed id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete Breed'));
        $this->viewBuilder()->setLayout('admin');

        $this->request->allowMethod(['post', 'delete']);
        $breed = $this->Breeds->get($id);
        if ($this->Breeds->delete($breed)) {
            $this->Flash->success(__('The breed has been deleted.'));
        } else {
            $this->Flash->error(__('The breed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
