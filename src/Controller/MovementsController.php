<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
/**
 * Movements Controller
 *
 * @property \App\Model\Table\MovementsTable $Movements
 * @property \App\Model\Table\PetsServicesTable $PetsServices
 *
 * @method \App\Model\Entity\Movement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MovementsController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Crud->addListener('Crud.Search', [
            // Events to listen for and apply search finder to query.
            'enabled' => [
                'Crud.beforeLookup',
                'Crud.beforePaginate'
            ],
            // Search collection to use
            'collection' => 'default'
        ]);
  
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'index'
            ],
            'listeners' => [
                'Crud.Search'
            ]
        ]);

    }
    public function isAuthorized($user) 
    {
        return true;    
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', __('Movement List'));
        $this->viewBuilder()->setLayout('admin');

        $this->loadModel('MovementTypes');
        $query2 = $this->MovementTypes->find('all');
        /*$movementTypes = $this->paginate($query2,['limit'=>'200']);
        $this->set('movementtypes', $movementTypes);*/
        $this->set('movementtypes');

        $query = $this->Movements->find()
            ->contain(['MovementTypes', 'Currencies', 'PetsServices.Services', 'PetsServices.Pets'])
            ->where(['currency_id IS NOT' => null])
            ->order(['movements__id'=>'DESC']);

        $q = $this->request->getQuery('q');
        $consulta = $this->request->getQuery('consulta');
        $desde = $this->request->getQuery('desde');
        $hasta = $this->request->getQuery('hasta');

        $this->set('desde', $desde);
        $this->set('hasta', $hasta);

        if ($q){
            $qc = '%'.$q.'%';
            $query = $query->where([
                                'OR' =>[['concept LIKE ' =>$qc], ['amount'=>$q]],
                                    ]);    
        }

        if ($consulta){
            if ($consulta>0){
                $query = $query->where(['movement_type_id ' => $consulta]);    
            }
        }

        if ($desde){
            $desde = str_replace('/', '-', $desde);
            $desde = date("Y-m-d",strtotime(date($desde)));
            $query = $query->where(['DATE(Movements.created) >=' => $desde]);    
        }

        if ($hasta){
            $hasta = str_replace('/', '-', $hasta);
            $hasta = date("Y-m-d",strtotime(date($hasta)));
            $query = $query->where(['DATE(Movements.created) <=' => $hasta]);    
        }

        $this->set(compact('MovementTypes'));
        $this->set('q', $q);
        $this->set('consulta', $consulta);

        
        $this->set('movements', $this->paginate($query));
    }

    /**
     * View method
     *
     * @param string|null $id Movement id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Listado de Movimientos');
        $this->viewBuilder()->setLayout('admin');

        $movement = $this->Movements->get($id, [
            'contain' => ['MovementTypes', 'Currencies', 'PetsServices.Services', 'PetsServices.Pets']
        ]);

        $this->set('movement', $movement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add Movement'));
        $this->viewBuilder()->setLayout('admin');

        $movement = $this->Movements->newEntity();
        if ($this->request->is('post')) {
            $movement = $this->Movements->patchEntity($movement, $this->request->getData());
            if ($this->Movements->save($movement)) {
                $this->Flash->success(__('The movement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movement could not be saved. Please, try again.'));
        }
        $movementTypes = $this->Movements->MovementTypes->find('list', ['limit' => 200]);
        $currencies = $this->Movements->Currencies->find('list', ['limit' => 200]);
        $petsServices = $this->Movements->PetsServices->find('list', ['limit' => 200]);
        $this->set(compact('movement', 'movementTypes', 'currencies', 'petsServices', 'services', 'pets'));
    }
    /**
     * Edit method
     *
     * @param string|null $id Movement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function edit($id = null)
    {
        $this->set('title', __('Modificar Movimiento'));
        $this->viewBuilder()->setLayout('admin');

        $movement = $this->Movements->get($id, [
            'contain' => ['PetsServices.Pets' , 'PetsServices.Services']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movement = $this->Movements->patchEntity($movement, $this->request->getData());
            if ($this->Movements->save($movement)) {
                $this->Flash->success(__('The movement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The movement could not be saved. Please, try again.'));
        }
        $movementTypes = $this->Movements->MovementTypes->find('list', ['limit' => 200]);
        $currencies = $this->Movements->Currencies->find('list', ['limit' => 200]);
        //$petsServices = $this->Movements->PetsServices->find('list', ['limit' => 200]);
        
        $arrfecha = (array) $movement->pets_service->start_date;
        $fecha = substr($arrfecha['date'],0,10);

        $pets = $this->Movements->PetsServices->Pets->find('list', ['limit' => 500]);

        $this->loadModel('Services');
        $services = $this->Services->find('list',['limit'=>'200']);
        $this->set('services', $services);
        
        //// Busqueda de Mascota asociada al petservice
        /*$pets = TableRegistry::get('PetsServices');
        $pets = $pets->find();
        $pets = $pets->contain('Pets');
        $pets = $pets->select(['Pets.id','Pets.name']);
        $pets->where(['DATE(start_date)'=>$fecha , 'pet_id' =>$movement->pets_service->pet_id]);
        $pets = $pets->toList();
        $pets = $pets[0];*/

        //// Busqueda de Servicio asociada al petservice de una Mascota
        /*$services = TableRegistry::get('PetsServices');
        $services = $services->find();
        $services = $services->contain(['Pets','Services']);
        $services->where(['DATE(start_date)'=>$fecha,'pet_id'=>$movement->pets_service->pet_id]);
        $services = $services->toList();
        $services = $services[0];*/

        $this->set(compact('movement', 'movementTypes', 'currencies', 'pets')); //, 'petsServices', 'services'
       
    }

    /**
     * Delete method
     *
     * @param string|null $id Movement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete Movement'));
        $this->viewBuilder()->setLayout('admin');
        
        $this->request->allowMethod(['post', 'delete']);
        $movement = $this->Movements->get($id);
        if ($this->Movements->delete($movement)) {
            $this->Flash->success(__('The movement has been deleted.'));
        } else {
            $this->Flash->error(__('The movement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*
    *
    * New method
    *
    *
    */
    public function getPets($fecha = null){
        if($fecha==null){
            $fecha=$this->request->data('fecha');
        }
        
        $f=explode('/', $fecha);
        $fecha = date("Y-m-d",strtotime($f[1].'/'.$f[0].'/'.$f[2]));

        $pets = TableRegistry::get('PetsServices');
        $pets = $pets->find('all');
        $pets = $pets->contain('Pets');
        $pets->where(['DATE(start_date)'=>$fecha]);
        echo json_encode($pets);
        die();
    }
    
    public function getServices() {
        $idpet = $this->request->data('idpet');
        $fecha = $this->request->data('fecha');
        $f=explode('/', $fecha);
        $fecha = date("Y-m-d",strtotime($f[1].'/'.$f[0].'/'.$f[2]));

        $pets = TableRegistry::get('PetsServices');
        $pets = $pets->find('all');
        $pets = $pets->contain(['Pets','Services']);
        $pets->where(['DATE(start_date)'=>$fecha,'pet_id'=>$idpet]);
        echo json_encode($pets);
        die();
    }
}
