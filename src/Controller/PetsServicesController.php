<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;

/**
 * PetsServices Controller
 *
 * @property \App\Model\Table\PetsServicesTable $PetsServices
 *
 * @method \App\Model\Entity\PetsService[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PetsServicesController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Crud->addListener('Crud.Search', [
            // Events to listen for and apply search finder to query.
            'enabled' => [
                'Crud.beforeLookup',
                'Crud.beforePaginate'
            ],
            // Search collection to use
            'collection' => 'default'
        ]);
        
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'index','add'
            ],
            'listeners' => [
                'Crud.Search'
            ]
        ]);
    }
    public function isAuthorized($user) 
    {
        //print_r($user); 
        if ($user['role_id'] == Configure::read('ROLES.ADMIN') || $user['role_id'] == Configure::read('ROLES.USER'))
            return true;

        // Default 
        return parent::isAuthorized($user);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($status = null)
    {
        
        $this->set('title', __('Pet Service List'));
        $this->viewBuilder()->setLayout('admin');

        $this->loadModel('Services');
        $services = $this->Services->find('all');
        $this->set(compact('services'));
        //$services = $this->paginate($query2);
        //$this->set('services', $services);


        $query = $this->PetsServices
            ->find('search', ['search' => $this->request->getQueryParams()])
            ->contain(['Pets', 'Services'])
            ->where(['pet_id IS NOT' => null]);
        
        $pagados = $this->request->getQuery('pagados');
        if ($pagados){
            $query = $query->where(['confirmed' => 0 ]);    
        }else{
            $pagados = 0;
        }

        $consulta = $this->request->getQuery('consulta');
        if ($consulta>0){
            $query = $query->where(['service_id' => $consulta ]);    
        }else{
            $consulta = 0;
        }
        
        
        $q = $this->request->getQuery('q');
        $this->set('q', $q);
        $this->set('pagados', $pagados);
        $this->set('consulta', $consulta);
        
        
        /*
        $petsServices = $this->paginate($query);
        
        $this->set(compact('petsServices'));*/
        $this->paginate = [
            'limit' => 10,
            'order' => [
                'id' => 'desc'
            ]
        ];
        
        $this->set('petsServices', $this->paginate($query));
        
        
        //$this->set(compact('petsServices'));
    }
    

    /**
     * View method
     *
     * @param string|null $id Pets Service id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', __('View Pet Service'));
        $this->viewBuilder()->setLayout('admin');
        
        $petsService = $this->PetsServices->get($id, [
            'contain' => ['Pets', 'Services', 'Pets.Users']
            //'contain' => ['Pets.PetsServices.Services','Pets.Breeds', 'Pets.Genders']
        ]);

        $this->set('petsService', $petsService);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', __('Add Pet Service'));
        $this->viewBuilder()->setLayout('admin');
        $petsService = $this->PetsServices->newEntity();
        if ($this->request->is('post')) {
            /*
            echo "<pre>";
            print_r($this->request->getData());
            die();
            */
            $valores = $this->request->getData();
            $pet_id =  $valores["pet_id"];
            $service_id = $valores["service_id"];
            $total_a = count($valores["start_date"]);

            for ($i = 0; $i < $total_a; $i++) {
                $start_date = $valores["start_date"][$i];
                $end_date = $valores["end_date"][$i];
                $confirmed = $valores["confirmed"][$i];

                $petsService->pet_id = $pet_id;
                $petsService->service_id = $service_id;
                $petsService->start_date = $start_date;
                $petsService->end_date = $end_date;
                $petsService->confirmed = $confirmed;

                if (!$this->PetsServices->save($petsService)) {
                    $this->Flash->error(__('El servicio para la mascota no pudo ser agregado. Por favor intente nuevamente.'));
                    return $this->redirect(['action' => 'add']);
                }
                $petsService = $this->PetsServices->newEntity();
            }
            return $this->redirect(['action' => 'index']);
        }
        //$pets = $this->PetsServices->Pets->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $services = $this->PetsServices->Services->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $pets = $this->PetsServices->Pets->find('',['order' => ['name' => 'ASC']])
                            ->select(['id', 'name', 'photo', 'photo_dir']);
        //$employees = $this->PetsServices->Employees->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $this->set(compact('petsService', 'pets', 'services', 'employees'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pets Service id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', __('Edit Pet Service'));
        $this->viewBuilder()->setLayout('admin');
        $petsService = $this->PetsServices->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            //print_r($this->request->getData());
            $petsService = $this->PetsServices->patchEntity($petsService, $this->request->getData());
            
            $fecha_start = $this->request->getData('start_date');
            
            $fecha_start = Time::createFromFormat(
                'd/m/y H:i A',
                $fecha_start,
                'America/Caracas'
            );
            $start_datef = date_format( $fecha_start, 'Y-m-d h:i');

            $fecha_end = $this->request->getData('end_date');
            $fecha_end = Time::createFromFormat(
                'd/m/y H:i A',
                $fecha_end,
                'America/Caracas'
            );
            $end_datef = date_format( $fecha_end, 'Y-m-d h:i');
            
            $petsService->start_date = $start_datef;
            $petsService->end_date = $end_datef;
            //die($petsService);
            
            if ($this->PetsServices->save($petsService)) {
                $this->Flash->success(__('The pets service has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El servicio para la mascota no pudo ser agregado. Por favor intente nuevamente.'));
        }
        //$pets = $this->PetsServices->Pets->find('list', ['limit' => 200]);
        $services = $this->PetsServices->Services->find('list', ['limit' => 200]);
        $pets = $this->PetsServices->Pets->find('',['order' => ['name' => 'ASC']])
                            ->select(['id', 'name', 'photo', 'photo_dir']);
        //$employees = $this->PetsServices->Employees->find('list', ['limit' => 200,'order'=>['name'=>'ASC']]);
        $this->set(compact('petsService', 'pets', 'services', 'employees'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pets Service id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->set('title', __('Delete Pet Service'));
        $this->viewBuilder()->setLayout('admin');
        
        $this->request->allowMethod(['post', 'delete']);
        $petsService = $this->PetsServices->get($id);
        if ($this->PetsServices->delete($petsService)) {
            $this->Flash->success(__('The pets service has been deleted.'));
        } else {
            $this->Flash->error(__('The pets service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
}
